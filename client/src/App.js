import React, { Component } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
// Landing Page
import Team from './Components/TeamManagement/team';
import FAQ from './Components/FAQ/faq';
import Home from './Components/Landing/Home';
import About from './Components/AboutUs/about';
import './App.css';
// Role
import OwnerLogin from './Components/ContractOwner/Ownerlogin';
import InspectorLogin from './Components/Inspector/inspectorLogin';
import UserLogin from './Components/Users/userLogin';

// Contract Owner
import AddLandInspector from './Components/ContractOwner/add';
import InspectorDirectory from './Components/ContractOwner/inspectordir';
import FeedbackDir from './Components/ContractOwner/feedback';

// Inspector
import VerifyLand from './Components/Inspector/verify';
import DetailPage from './Components/Inspector/detail';

// Users Services
import UserHome from './Components/Users/userHome';
import LandReg from './Components/Users/landreg';
import Gallery from './Components/Users/gallery';
import Profile from './Components/Users/profile';

// Vote
import VoteInspector from './Components/Users/vote';

// //feedback
// import UserBody from './Components/UserBody/UserBody'; // Import UserBody component



class App extends Component {
  render() {
    return (
      <Router>
        <div>

          <Routes>
            {/* Landing Page */}
            <Route path="/" element={<Home />} />
            <Route path="/team-management" element={<Team />} />
            <Route path="/about-us" element={<About />} />
            <Route path="/faq" element={<FAQ />} />
            {/* Role */}
            <Route path="/contract-owner" element={<OwnerLogin />} />
            <Route path='/land-inspector' element={<InspectorLogin />} />
            <Route path='/land-deals' element={<UserLogin />} />
            {/* Contract Owner */}
            <Route path='/add-inspector' element={<AddLandInspector />} />
            <Route path='/inspector-directory' element={<InspectorDirectory />} />
            <Route path='/feedback-directory' element={<FeedbackDir />} />
            {/* Land Inspector */}
            <Route path='/verify' element={<VerifyLand />} />
            <Route path="/detail/:thramNumber" element={<DetailPage/>} />
            {/* User Services */}
            <Route path='/user-home' element={<UserHome />} />
            <Route path='/land-reg' element={<LandReg />} />
            <Route path='/land-gallery' element={<Gallery />} />
            <Route path='/user-profile' element={<Profile />} />
            {/* Vote */}
            <Route path='/vote' element={<VoteInspector />} />

           
          </Routes>
        </div>
      </Router>
    );
  }
}

export default App;
