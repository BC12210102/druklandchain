import React from "react";
import Navigation from "../Landing/Navigation";
import logo from './image/footerlogo.png'
import UserBody from "./userBody";
import "./css/userHome.css"


const UserHome = () => {
    return (
        <div className="user-home-main">
            <Navigation />
            <div className='logo'>
                <img src={logo} className="logo" alt='logo' height={50} />

            </div>
            <div className='user_home_text'>
                <p>
                    “Druk LandChain DAO provides legal documentation and protection, ensuring rightful ownership and facilitating transparent transactions."
                </p>
            </div>
            <div className="user-body">
                <UserBody/>
            </div>

        </div>
    )
}
export default UserHome;