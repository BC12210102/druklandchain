import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Web3 from "web3";
import axios from "axios";
import { LandChain_ABI, LandChain } from "../../abi/landChain";
import "./css/landreg.css";
import Swal from "sweetalert2";
import AOS from "aos";
import "aos/dist/aos.css";

const PINATA_API_KEY = "8143e82da885a5af6a9b";
const PINATA_SECRET_API_KEY = "9aa2abf6e79b93e868147cc21e5bedfa40755a8c4538a6d92a52a5dd248151a3";

const LandReg = () => {
    const [thramNumber, setThramNumber] = useState("");
    const [plotNumber, setPlotNumber] = useState("");
    const [totalArea, setTotalArea] = useState("");
    const [location, setLocation] = useState("");
    const [cadestralFile, setCadestralFile] = useState(null);
    const [landPictureFile, setLandPictureFile] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        AOS.init({
            duration: 1000, // Animation duration
            once: true,     // Whether animation should happen only once - while scrolling down
        });
    }, []);

    const handleFileUpload = async (file) => {
        const formData = new FormData();
        formData.append("file", file);

        const metadata = JSON.stringify({
            name: file.name,
        });
        formData.append("pinataMetadata", metadata);

        const options = JSON.stringify({
            cidVersion: 0,
        });
        formData.append("pinataOptions", options);

        try {
            const response = await axios.post("https://api.pinata.cloud/pinning/pinFileToIPFS", formData, {
                maxBodyLength: "Infinity",
                headers: {
                    "Content-Type": `multipart/form-data; boundary=${formData._boundary}`,
                    pinata_api_key: PINATA_API_KEY,
                    pinata_secret_api_key: PINATA_SECRET_API_KEY,
                },
            });
            return response.data.IpfsHash;
        } catch (error) {
            console.error("Error uploading file to Pinata: ", error);
        }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (!window.ethereum) {
            Swal.fire({
                icon: 'error',
                title: 'MetaMask is not installed',
                text: 'Please install MetaMask to proceed.',
            });
            return;
        }

        const web3 = new Web3(window.ethereum);
        try {
            setLoading(true);
            await window.ethereum.enable();
            const accounts = await web3.eth.getAccounts();
            const account = accounts[0];
            const cadestralHash = await handleFileUpload(cadestralFile);
            const landPictureHash = await handleFileUpload(landPictureFile);
            const landChainContract = new web3.eth.Contract(LandChain_ABI, LandChain);
            await landChainContract.methods.addLand(
                parseInt(thramNumber),
                parseInt(plotNumber),
                location,
                parseInt(totalArea),
                cadestralHash,
                landPictureHash
            ).send({ from: account });

            Swal.fire({
                icon: 'success',
                title: 'Land registered successfully!',
                backdrop: `rgba(0,0,0,0.7)`
            });
        } catch (error) {
            console.error("Error registering land: ", error);
            Swal.fire({
                icon: 'error',
                title: 'Error registering land',
                text: 'An error occurred while registering land. Please try again later.',
                backdrop: `rgba(0,0,0,0.7)`
            });
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className="land-reg-div" data-aos="fade-up">
            <Link to="/user-home">
                <button className="back-button">Back</button>
            </Link>
            {loading && (
                <div className="loader-overlay">
                    <div className="loader"></div>
                </div>
            )}
            <form className={`land-reg-form ${loading ? "blur-background" : ""}`} onSubmit={handleSubmit}>
                <h2>Register your land</h2>
                <label>Thram Number:</label>
                <input
                    type="number"
                    placeholder="Enter Thram Number"
                    value={thramNumber}
                    onChange={(e) => setThramNumber(e.target.value)}
                />
                <label>Plot Number:</label>
                <input
                    type="number"
                    placeholder="Enter Plot Number"
                    value={plotNumber}
                    onChange={(e) => setPlotNumber(e.target.value)}
                />
                <label>Total Area of Land (in square meters):</label>
                <input
                    type="number"
                    placeholder="Enter Total Area of Land"
                    value={totalArea}
                    onChange={(e) => setTotalArea(e.target.value)}
                />
                <label>Location of Land:</label>
                <input
                    type="text"
                    placeholder="Enter Location of Land"
                    value={location}
                    onChange={(e) => setLocation(e.target.value)}
                />
                <label htmlFor="cadestral-file" className="file-upload-button">
                    Upload Your Cadestral Map
                </label>
                <input
                    id="cadestral-file"
                    type="file"
                    accept="image/*"
                    onChange={(e) => setCadestralFile(e.target.files[0])}
                />
                <label htmlFor="land-picture-file" className="file-upload-button">
                    Upload Your Land Picture
                </label>
                <input
                    id="land-picture-file"
                    type="file"
                    accept="image/*"
                    onChange={(e) => setLandPictureFile(e.target.files[0])}
                />
                <button type="submit">Register</button>
            </form>
        </div>
    );
};

export default LandReg;
