import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./css/vote.css";
import Web3 from "web3";
import Swal from "sweetalert2";
import { LandChain_ABI, LandChain } from "../../abi/landChain";

// Loader component
const Loader = () => {
  return (
    <div className="loader-overlay">
      <div className="loader"></div>
    </div>
  );
};

const VoteInspector = () => {
  const [inspectors, setInspectors] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");

  useEffect(() => {
    const fetchInspectors = async () => {
      setLoading(true);
      if (window.ethereum) {
        const web3 = new Web3(window.ethereum);
        try {
          await window.ethereum.enable();
          const accounts = await web3.eth.getAccounts();
          const account = accounts[0];
          const landChainContract = new web3.eth.Contract(LandChain_ABI, LandChain);
          const inspectorList = await landChainContract.methods.getAllInspectors().call({ from: account });
          setInspectors(inspectorList);
        } catch (error) {
          setError("Error fetching inspectors. Please try again later.");
          console.error("Error fetching inspectors:", error);
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Error fetching inspectors. Please try again later.',
            backdrop: `rgba(0,0,0,0.7)`
          });
        }
        setLoading(false);
      } else {
        setError("MetaMask is not installed. Please install MetaMask to proceed.");
        Swal.fire({
          icon: 'error',
          title: 'MetaMask Not Installed',
          text: 'Please install MetaMask to interact with the Ethereum network.',
          backdrop: `rgba(0,0,0,0.7)`
        });
        setLoading(false);
      }
    };

    fetchInspectors();
  }, []);

  const voteForInspector = async (inspectorAddr) => {
    if (window.ethereum) {
      const web3 = new Web3(window.ethereum);
      try {
        setLoading(true);
        await window.ethereum.enable();
        const accounts = await web3.eth.getAccounts();
        const account = accounts[0];
        const landChainContract = new web3.eth.Contract(LandChain_ABI, LandChain);

        const votePrice = await landChainContract.methods.VOTE_PRICE().call();

        await landChainContract.methods.voteInspector(inspectorAddr).send({
          from: account,
          value: votePrice
        });

        setMessage("Vote cast successfully!");
        Swal.fire({
          icon: 'success',
          title: 'Success',
          text: 'Vote cast successfully!',
          backdrop: `rgba(0,0,0,0.7)`
        });

        const inspectorList = await landChainContract.methods.getAllInspectors().call({ from: account });
        setInspectors(inspectorList);

      } catch (error) {
        setError("Error voting for inspector. Please try again later.");
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Error voting for inspector. Please try again later.',
          backdrop: `rgba(0,0,0,0.7)`
        });
        console.error("Error voting for inspector:", error);
      } finally {
        setLoading(false); 
      }
    } else {
      setError("MetaMask is not installed. Please install MetaMask to proceed.");
      Swal.fire({
        icon: 'error',
        title: 'MetaMask Not Installed',
        text: 'Please install MetaMask to interact with the Ethereum network.',
        backdrop: `rgba(0,0,0,0.7)`
      });
    }
  };

  return (
    <div className="user-vote-main">
      <h2>Vote For Land Inspector</h2>
      <Link to="/user-home">
        <button className="back-vote">Back</button>
      </Link>
      {loading && <Loader />}
      <table className={`inspector-vote-table ${loading ? "blur-background" : ""}`}>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Wallet Address</th>
            <th>Email</th>
            <th>Vote Count</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {inspectors.map((inspector, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{inspector.name}</td>
              <td>{inspector.walletAddr}</td>
              <td>{inspector.email}</td>
              <td>{inspector.voteCount}</td>
              <td>
                <button
                  className="action-button vote-button"
                  onClick={() => voteForInspector(inspector.walletAddr)}
                >
                  Vote
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default VoteInspector;
