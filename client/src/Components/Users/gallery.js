import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Web3 from "web3";
import { LandChain_ABI, LandChain } from "../../abi/landChain";
import "./css/gallery.css";
import Swal from 'sweetalert2';
import AOS from "aos";
import "aos/dist/aos.css";

const Gallery = () => {
    const [verifiedLands, setVerifiedLands] = useState([]);
    const [error, setError] = useState(null);
    const [filteredLands, setFilteredLands] = useState([]);
    const [contract, setContract] = useState(null);
    const [loading, setLoading] = useState(true);
    const [buyingLand, setBuyingLand] = useState(false);
    const [currentThramNumber, setCurrentThramNumber] = useState(null);
    const [connectedWallet, setConnectedWallet] = useState("");
    const [logoutLoading, setLogoutLoading] = useState(false); // Added state for logout loading

    useEffect(() => {
        AOS.init({
            duration: 1000, // Animation duration
            once: true,     // Whether animation should happen only once - while scrolling down
        });
    }, []);

    useEffect(() => {
        const fetchVerifiedLands = async () => {
            if (window.ethereum) {
                const web3 = new Web3(window.ethereum);

                try {
                    await window.ethereum.request({ method: 'eth_requestAccounts' });
                    const accounts = await web3.eth.getAccounts();
                    const ownerAddress = accounts[0];
                    setConnectedWallet(ownerAddress);

                    const landChainContract = new web3.eth.Contract(LandChain_ABI, LandChain);
                    setContract(landChainContract);

                    const allVerifiedLands = await landChainContract.methods.getVerifiedLands().call();
                    const verifiedLandsFilteredByOwner = allVerifiedLands.filter(land => land.owner.toLowerCase() !== ownerAddress.toLowerCase());
                    setVerifiedLands(verifiedLandsFilteredByOwner);
                    setFilteredLands(verifiedLandsFilteredByOwner);
                    setLoading(false); 
                } catch (error) {
                    console.error("Error fetching verified land details: ", error);
                    setError("Failed to load land details. Please try again later.");
                    setLoading(false); 
                }
            } else {
                console.error("MetaMask is not installed.");
                setError("MetaMask is not installed. Please install MetaMask to use this feature.");
                setLoading(false); 
            }
        };

        fetchVerifiedLands();
    }, []);

    const convertWeiToEther = (priceInWei) => {
        const web3 = new Web3();
        return web3.utils.fromWei(priceInWei, 'ether');
    };

    const checkLandBought = async (thramNumber) => {
        if (!contract) return false;
        const isBought = await contract.methods.checkLandBought(thramNumber).call();
        return isBought;
    };

    const handleBuyLand = async (thramNumber, priceInWei) => {
        try {
            if (contract) {
                setBuyingLand(true);
                setCurrentThramNumber(thramNumber);
                const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
                const web3 = new Web3(window.ethereum);
                const priceInWeiBN = web3.utils.toBN(priceInWei);
                let isVerified = await contract.methods.isLandVerified(thramNumber).call({ from: accounts[0] });

                if (!isVerified) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Land Not Verified',
                        text: 'The selected land is not verified.',
                    });
                    setBuyingLand(false);
                    setCurrentThramNumber(null);
                    return;
                }

                const isBought = await checkLandBought(thramNumber);
                if (isBought) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Land Already Purchased',
                        text: 'The selected land has already been purchased.',
                    });
                    setBuyingLand(false);
                    setCurrentThramNumber(null);
                    return;
                }

                await contract.methods.buyLand(thramNumber).send({ from: accounts[0], value: priceInWei });
                const updatedFilteredLands = filteredLands.filter(land => land.thramNumber !== thramNumber);
                setFilteredLands(updatedFilteredLands);
                Swal.fire({
                    icon: 'success',
                    title: 'Purchase Successful',
                    text: `You have successfully purchased the land with ID ${thramNumber}.`,
                });
            } else {
                console.error("Contract instance not available.");
                setError("Contract instance not available. Please try again later.");
            }
        } catch (error) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: error.message || 'Failed to purchase land',
            });
            console.error("Error purchasing land: ", error);
        } finally {
            setBuyingLand(false);
            setCurrentThramNumber(null);
        }
    };

    const formatWalletAddress = (address) => {
        return address ? `${address.slice(0, 6)}...${address.slice(-4)}` : "Not connected";
    };

    const handleLogout = async () => {
        try {
            setLogoutLoading(true); 
            setConnectedWallet("");
            Swal.fire({
                title: "Success",
                text: "You have successfully logged out.",
                icon: "success",
                showConfirmButton: false,
                timer: 1500
            }).then(() => {
                window.location.href = "/";
            });
        } catch (error) {
            console.error("Error logging out: ", error);
        } finally {
            setLogoutLoading(false); 
        }
    };

    if (loading) {
        return <div className="loader"></div>;
    }

    if (error) {
        return <div className="error-message">{error}</div>;
    }
    return (
        <div className="user-profile-div" data-aos="fade-up">
            <div className="profile-header">
                <img src="http://localhost:3000/static/media/footerlogo.20b6060e.png" alt="Logo" />
                <button onClick={handleLogout} disabled={logoutLoading}>
                    {logoutLoading ? <div className="loader"></div> : "Log Out"}
                </button>
                </div>
            <div className="profile-hero">
                <img src="https://i.pinimg.com/564x/3e/47/88/3e4788fa9553514e6fc8ed56d26b047b.jpg" alt="Hero" />
                <div className="hero-text">
                    <p>“ Welcome to our unique and extensive online LandGallery. All work here is inspired by landscape and nature and can be purchased securely online today. Our aim is to offer you important and quality artwork by Britain's leading artists, with good investment value at the best prices.</p>
                </div>
            </div>
            <div className="user-detail-header">
                <div className="wallet">
                    <p>Connected to: {formatWalletAddress(connectedWallet)}</p>
                </div>
                <Link to="/user-home">
                    <button>Back</button>
                </Link>
            </div>
            {buyingLand && (
                <div className="loader-overlay">
                    <div className="loader"></div>
                </div>
            )}
            <div className={`gallery ${buyingLand ? "blur-background" : ""}`}>
                {filteredLands.length > 0 ? (
                    filteredLands
                    .filter(land => !land.purchased)
                    .map((land, index) => (
                        <div className="land-card" key={index} data-aos="fade-up">
                            <img src={`https://ipfs.io/ipfs/${land.imagehash}`} alt="Land"
                                onError={(e) => e.target.src = '/path/to/default-image.jpg'} />
                            <div className="land-info">
                                <p><strong>Owner:</strong><br />{land.owner}</p>
                                <p><strong>Location:</strong><br /> {land.location}</p>
                                <p><strong>Total Area:</strong><br /> {land.totalArea} sqm</p>
                                <p><strong>Price:</strong> <br />{convertWeiToEther(land.price)} ETH</p><br />
                                {buyingLand && currentThramNumber === land.thramNumber ? (
                                    <div className="loader"></div>
                                ) : (
                                    <button onClick={() => handleBuyLand(land.thramNumber, land.price)} className="buy-button">Buy</button>
                                )}
                            </div>
                        </div>
                    ))
                ) : (
                    <p>No available lands for sale at the moment.</p>
                )}
            </div>
            <div className="profile-footer">
                <p>©2024 DrukLandChain, Bhutan</p>
            </div>
        </div>
    );
};

export default Gallery;

