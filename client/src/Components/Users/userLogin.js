import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom"; // Import Link from react-router-dom
import Swal from 'sweetalert2';
import logo from "./image/logo.png";
import "./css/userLogin.css";
import metamaskImage from "./image/metamask.png";
import smallImage from "./image/metamask.png";
import UserHome from "./userHome";
import AOS from 'aos';
import 'aos/dist/aos.css';

function UserLogin() {
  const [loggedIn, setLoggedIn] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    AOS.init({
      duration: 1000, // Duration of the animation
      easing: 'ease-in-out', // Easing option
      once: true // Animation only occurs once
    });
  }, []);

  const handleConnectMetamask = async () => {
    try {
      setLoading(true);
      await window.ethereum.request({ method: 'eth_requestAccounts' });
      setLoading(false);
      setLoggedIn(true);

      Swal.fire({
        icon: 'success',
        title: 'Logged In',
        text: 'You have successfully logged in.',
        showConfirmButton: false,
        timer: 2000,
        backdrop: `rgba(0,0,0,0.7)`,
      });
    } catch (error) {
      setLoading(false);
      console.error("Error connecting to Metamask:", error);
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Error connecting to Metamask. Please try again.',
        backdrop: `rgba(0,0,0,0.7)`,
      });
    } finally {
      setLoading(false);
    }
  };

  if (loggedIn) {
    return <UserHome />;
  }

  return (
    <div className="container-box" data-aos="fade-up">
      <div className="logo-container">
        <img src={logo} alt="Your Logo" className="logo-owner" />
        <p className="p">Welcome Land Dealers and Buyers</p>
        <img src={metamaskImage} alt="Metamask" className="metamask-image" />
        <button onClick={handleConnectMetamask} className="connect-button">
          <img src={smallImage} alt="Small Image" className="small-image" />
          Connect to Metamask
        </button>
      </div>
      <Link to="/" className="home-link">
        <button>Go Back</button>
      </Link>
    </div>
  );
}

export default UserLogin;
