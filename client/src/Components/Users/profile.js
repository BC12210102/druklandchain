import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Web3 from "web3";
import { LandChain_ABI, LandChain } from "../../abi/landChain";
import "./css/profile.css";
import Swal from "sweetalert2";
import AOS from "aos";
import "aos/dist/aos.css";

const Profile = () => {
    const [connectedWallet, setConnectedWallet] = useState("");
    const [ownedLands, setOwnedLands] = useState([]);
    const [error, setError] = useState(null);
    const [logoutLoading, setLogoutLoading] = useState(false);

    useEffect(() => {
        AOS.init({
            duration: 1000, // Animation duration
            once: true,     // Whether animation should happen only once - while scrolling down
        });
    }, []);

    useEffect(() => {
        const fetchOwnedLands = async () => {
            setError(null);
            try {
                if (window.ethereum) {
                    const web3 = new Web3(window.ethereum);
                    await window.ethereum.request({ method: 'eth_requestAccounts' });
                    const accounts = await web3.eth.getAccounts();
                    setConnectedWallet(accounts[0]);

                    const ownerAddress = accounts[0];
                    const landChainContract = new web3.eth.Contract(LandChain_ABI, LandChain);

                    const allVerifiedLands = await landChainContract.methods.getVerifiedLands().call();
                    const landsOwnedByOwner = allVerifiedLands.filter(land => land.owner.toLowerCase() === ownerAddress.toLowerCase());

                    setOwnedLands(landsOwnedByOwner);
                } else {
                    throw new Error("MetaMask is not installed. Please install MetaMask to use this feature.");
                }
            } catch (error) {
                console.error("Error fetching owned land details: ", error);
                setError("Failed to load owned land details. Please try again later.");
            } 
        };

        fetchOwnedLands();
    }, []);

    const formatWalletAddress = (walletAddress) => {
        return `${walletAddress.slice(0, 6)}...${walletAddress.slice(-4)}`;
    };

    const convertWeiToEther = (priceInWei) => {
        const web3 = new Web3();
        return web3.utils.fromWei(priceInWei, 'ether');
    };

    const handleLogout = async () => {
        setLogoutLoading(true);
        try {
            await window.ethereum.request({ method: 'eth_requestAccounts' });
            setConnectedWallet("");
            setOwnedLands([]);
            Swal.fire({
                title: "Success",
                text: "You have successfully logged out.",
                icon: "success",
                showConfirmButton: false,
                timer: 1500
            }).then(() => {
                window.location.href = "/";
            });
        } catch (error) {
            console.error("Error logging out: ", error);
            setLogoutLoading(false);
        }
    };

    useEffect(() => {
        window.ethereum.on('accountsChanged', handleLogout);
        return () => {
            window.ethereum.removeListener('accountsChanged', handleLogout);
        };
    }, []);

    if (error) {
        return <div className="error-message" data-aos="fade-in">{error}</div>;
    }

    return (
        <div className="user-profile-div" data-aos="fade-up">
            <div className="profile-header" data-aos="fade-down">
                <img src="http://localhost:3000/static/media/footerlogo.20b6060e.png" alt="Logo" />
                <button onClick={handleLogout}>Log Out</button>
            </div>
            <div className="profile-hero" data-aos="fade-up">
                <img src="https://i.pinimg.com/564x/94/39/d2/9439d28c2485b8856f5994c4f261287b.jpg" alt="Hero" />
                <div className="hero-text">
                    <p>“ A landowner is not merely one who possesses vast acres of earth, but a custodian of nature's legacy, a steward of landscapes, and a guardian of the future's heritage. "</p>
                </div>
            </div>
            <div className="user-detail-header" data-aos="fade-down">
                <div className="wallet">
                    <p>Connected to: {formatWalletAddress(connectedWallet)}</p>
                </div>
                <Link to="/user-home">
                    <button>Back</button>
                </Link>
            </div>
            <div className="profile-title" data-aos="fade-up">
                <h2>Your Land</h2>
            </div>
            <div className="land-profile" data-aos="fade-up">
                {ownedLands.length > 0 ? (
                    ownedLands.map((land, index) => (
                        <div className="land-card" key={index} data-aos="fade-in">
                            <img src={`https://ipfs.io/ipfs/${land.imagehash}`} alt="Land" />
                            <div className="land-info">
                                <p>Thram Number: {land.thramNumber}</p>
                                <br />
                                <p>Plot Number: {land.plotNumber}</p>
                                <br />
                                <p>Location: {land.location}</p>
                                <br />
                                <p>Total Area: {land.totalArea} sqm</p>
                                <br />
                                <p>Owner: {formatWalletAddress(land.owner)}</p>
                                <br />
                                <p>Price: {convertWeiToEther(land.price)} ETH</p>
                            </div>
                        </div>
                    ))
                ) : (
                    <p>No owned lands available</p>
                )}
            </div>
            <div className="profile-footer" data-aos="fade-up">
                <p>©2024 DrukLandChain, Bhutan</p>
            </div>
        </div>
    );
};

export default Profile;
