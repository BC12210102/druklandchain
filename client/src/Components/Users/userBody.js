import React, { useEffect } from "react";
import { Link } from 'react-router-dom';
import Footer from "../Landing/footer";
import "./css/userbody.css";
import AOS from 'aos';
import 'aos/dist/aos.css';

const UserBody = ({connectedAccount}) => {
    useEffect(() => {
        AOS.init({
            duration: 1000, 
            easing: 'ease-in-out', 
        });
    }, []);

    // const submitFeedback = async () => {
    //     try {
    //         const response = await fetch('/feedback', {
    //             method: 'POST',
    //             headers: {
    //                 'Content-Type': 'application/json'
    //             },
    //             body: JSON.stringify({ feedback })
    //         });
    //         if (response.ok) {
    //             alert('Feedback submitted successfully!');
    //             setFeedback('');
    //         } else {
    //             throw new Error('Failed to submit feedback');
    //         }
    //     } catch (error) {
    //         console.error('Error submitting feedback:', error);
    //         alert('Failed to submit feedback. Please try again.');
    //     }
    // };

    return (
        <div className="user-body-main" data-aos="fade-up">
            <p>Welcome: {connectedAccount}</p>
            <div className="user-services">
                <div className="land-reg">
                    <div className="land-reg-logo">
                        <img src="https://i.pinimg.com/564x/d0/48/a9/d048a9a2800100381271a6bdcc622d49.jpg" alt="#" />
                    </div>
                    <Link to="/land-reg" style={{ textDecoration: 'none' }}>
                        <button className="land-reg">Register Land Info</button>
                    </Link>
                </div>
                <div className="land-gallery">
                    <div className="land-gallery-logo">
                        <img src="https://i.pinimg.com/564x/cc/80/4f/cc804f533ae37c8969ed5d095c3bf083.jpg" alt="#" />
                    </div>
                    <Link to="/land-gallery" style={{ textDecoration: 'none' }}>
                        <button className="land-reg">Land Gallery</button>
                    </Link>
                </div>
                <div className="user-profile">
                    <div className="user-profile-logo">
                        <img src="https://i.pinimg.com/564x/7b/12/d2/7b12d287221c0adf5b4efcdf326c178f.jpg" alt="#" />
                    </div>
                    <Link to="/user-profile" style={{ textDecoration: 'none' }}>
                        <button className="land-reg">Profile</button>
                    </Link>
                </div>
            </div>
            {/* <div className="user-feedback-div">
                <h2>Have any feedback?</h2>
                <input type="text" placeholder="Write Feedback..." />
            </div> */}
            <div className="vote-div">
                <div className="vote-logo">
                    <img src="https://i.pinimg.com/564x/3d/4a/32/3d4a32521a029007416b4aedf595e681.jpg" alt="#" />
                    <span> Vote Your Land inspector</span>
                </div>
                <div className="vote">
                    <p>"Your vote for the land inspector shapes the future of our community's land management and development."</p>
                    <Link to="/vote">
                        <button>Vote Inspector</button>
                    </Link>
                </div>
            </div>
            <div className="token-div">
            </div>
            <div className="user-footer" data-aos="fade-up">
                <Footer />
            </div>
        </div>
    );
};

export default UserBody;
