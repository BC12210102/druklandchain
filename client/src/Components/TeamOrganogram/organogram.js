import React from "react";
import './organogram.css';

const Organogram = () => {
    return (
        
            <div className="organo-chart">
                <h2>Team Organogram</h2>
                <img src="https://previews.dropbox.com/p/thumb/ACSSTm8cat9A5Ft574XUQvIUt2aEBF7uULukiuRTGQYNapUJbpEIQTjdMbTrBzG2HhxxVdlsJt_LhGCQGS6fRKttRIkWt_HXvbdpyKA-wsOuOuxrI8icV2OcS0dulxqqrLGhWqEMxllcouLeyWwGbsFSSId61t1EH_g1PFKFi5yGSVhRcR6TKTXKBsLvVwzyhH25DTjhyml0hr5mFLpwv5zExC3kI1uUfs91N0sDLjrnP2NBEac0oyGxvkxwCFoIZrwFTiMYonj6Uy1UWbx16lSYQpaGYp849TKFocP6aCKvIVQg0MX-MkqOv9xOwYaQfHwRtCzZXL4sdL7FYnLwyX98/p.png" alt="#"></img>
            </div>
        
    );
}
export default Organogram;