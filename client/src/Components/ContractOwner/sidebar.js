import React from 'react';
import { Link } from 'react-router-dom';
import "./css/sidebar.css"
import Swal from 'sweetalert2';


const Sidebar = () => {
  const handleLogout = () => {
    Swal.fire({
      icon: 'success',
      title: 'Logged Out',
      text: 'You have successfully logged out.',
      backdrop: `rgba(0,0,0,0.7)`,
      showConfirmButton: false,
      timer: 2000 
    }).then(() => {
      window.location.href = '/';
    });
  };
  return (
    <div>
      <nav className='owner-navigation'>
        <ul className='owner-ul'>
          <li className='owner-li'>
            <img src='https://i.pinimg.com/564x/05/1a/a7/051aa79c5e3fbdf9d479ddbfd46d502f.jpg' alt='#' />
            <Link to="/add-inspector">Add Land Inspector</Link>
          </li>
          <li className='owner-li'>

            <img src='https://i.pinimg.com/564x/b0/e1/6b/b0e16beff90d20a1bd412d9a2a6e79bd.jpg' alt='#' />
            <Link to="/inspector-directory">Inspector Catalogue</Link>
          </li>
          <li className='owner-li'>
            <img src='https://i.pinimg.com/564x/e0/8e/57/e08e5711c93bcf905ef1ce94617f8828.jpg' alt='#' />
            <Link to="/feedback-directory">Review Catalogue</Link>
          </li>
          <li className='owner-li'>
            <img src='https://i.pinimg.com/564x/42/92/e0/4292e0ea4b8e1fa3b969774d6b880314.jpg' alt='#' />
            <Link onClick={handleLogout}>Log Out</Link>
          </li>
        </ul>
      </nav>

    </div>

  );
};

export default Sidebar;
