import React, { useState, useEffect } from "react";
import Sidebar from "./sidebar";
import "./css/feedback.css";
import { LandChain_ABI, LandChain } from "../../abi/landChain";
import Web3 from "web3";

function Loader() {
    return (
        <div className="loader-overlay">
            <div className="loader"></div>
        </div>
    );
}

function FeedbackDir() {
    const [inspectorsCount, setInspectorsCount] = useState(0);

    useEffect(() => {
        const fetchInspectorsCount = async () => {
            if (window.ethereum) {
                const web3 = new Web3(window.ethereum);
                try {
                    await window.ethereum.enable();
                    const accounts = await web3.eth.getAccounts();
                    const account = accounts[0];
                    const landChainContract = new web3.eth.Contract(
                        LandChain_ABI,
                        LandChain
                    );
                    const inspectorCount = await landChainContract.methods
                        .getInspectorsCount()
                        .call({ from: account });
                    setInspectorsCount(inspectorCount);
                } catch (error) {
                    console.error("Error fetching inspectors count:", error);
                }
            } else {
                console.error("MetaMask is not installed.");
            }
        };

        fetchInspectorsCount();
    }, []);

    return (
        <div>
            <div className='side-bar-header'>
                <h2>Contract Owner Dashboard</h2>
            </div>
            <Sidebar />

            <div className="feedback-container">
                <div className="stats">
                    <h2>Yearly Performance</h2>
                    <h2>Total Land Inspectors: {inspectorsCount}</h2>
                </div>
                <h2>Feedbacks</h2>
                <div className="feedback-div">
                    <p>The navigation is intuitive and easy to use. I could find what I needed quickly.</p>
                </div>
                <div className="feedback-div">
                    <p>The navigation is intuitive and easy to use. I could find what I needed quickly.</p>
                </div>
                <div className="feedback-div">
                    <p>The navigation is intuitive and easy to use. I could find what I needed quickly.</p>
                </div>
                <div className="feedback-div">
                    <p>The navigation is intuitive and easy to use. I could find what I needed quickly.</p>
                </div>
                <div className="feedback-div">
                    <p>The navigation is intuitive and easy to use. I could find what I needed quickly.</p>
                </div>
                <div className="feedback-div">
                    <p>The navigation is intuitive and easy to use. I could find what I needed quickly.</p>
                </div>
            </div>
        </div>
    );
}

export default FeedbackDir;
