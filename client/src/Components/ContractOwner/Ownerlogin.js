import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom"; // Import Link from react-router-dom
import logo from "./image/logo.png";
import "./css/OwnerLogin.css";
import metamaskImage from "./image/metamask.png";
import smallImage from "./image/metamask.png";
import AddLandInspector from "./add";
import Swal from 'sweetalert2';
import Web3 from "web3";
import { LandChain_ABI, LandChain } from "../../abi/landChain";
import AOS from 'aos';
import 'aos/dist/aos.css';

function OwnerLogin() {
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    AOS.init({
      duration: 1000, // Duration of the animation
      easing: 'ease-in-out', // Easing option
      once: true // Animation only occurs once
    });
  }, []);

  const handleConnectMetamask = async () => {
    try {
      await window.ethereum.request({ method: 'eth_requestAccounts' });
      const web3 = new Web3(window.ethereum);
      const accounts = await web3.eth.getAccounts();
      const contract = new web3.eth.Contract(LandChain_ABI, LandChain);
      const contractOwner = await contract.methods.contractOwner().call();
      if (accounts[0].toLowerCase() === contractOwner.toLowerCase()) {
        setLoggedIn(true);
        Swal.fire({
          icon: 'success',
          title: 'Success',
          text: 'You have successfully logged in.',
          backdrop: `rgba(0,0,0,0.7)` 
        });
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Access Denied',
          text: 'Only contract owner can login.',
          backdrop: `rgba(0,0,0,0.7)` 
        });
        console.log("Connected account is not the contract owner");
      }
    } catch (error) {
      console.error("Error connecting to Metamask:", error);
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'An error occurred while connecting to Metamask. Please try again later.',
        backdrop: `rgba(0,0,0,0.7)` 
      });
    }
  };
  
  if (loggedIn) {
    return <AddLandInspector />;
  }

  return (
    <div className="container-box" data-aos="fade-up">
      <div className="logo-container">
        <img src={logo} alt="Your Logo" className="logo-owner" />
        <p className="p">Welcome Contract Owner</p>
        <img src={metamaskImage} alt="Metamask" className="metamask-image" />
        <button onClick={handleConnectMetamask} className="connect-button">
          <img src={smallImage} alt="Small Image" className="small-image" />
          Connect to Metamask
        </button>
      </div>
      <Link to="/" className="home-link">
        <button>Go Back</button>
      </Link>
    </div>
  );
}

export default OwnerLogin;
