import React, { useState } from "react";
import Sidebar from "./sidebar";
import "./css/add.css";
import Swal from 'sweetalert2';
import Web3 from "web3";
import { LandChain_ABI, LandChain } from "../../abi/landChain";

function AddLandInspector() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [walletAddress, setWalletAddress] = useState("");
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    if (window.ethereum) {
      const web3 = new Web3(window.ethereum);
      try {
        await window.ethereum.enable();

        const accounts = await web3.eth.getAccounts();
        const account = accounts[0];

        const landChainContract = new web3.eth.Contract(
          LandChain_ABI,
          LandChain
        );
        await landChainContract.methods
          .addLandInspector(name, email, walletAddress)
          .send({ from: account });
        setName("");
        setEmail("");
        setWalletAddress("");
        Swal.fire({
          icon: 'success',
          title: 'Success',
          text: 'Land inspector added successfully!',
          backdrop: `rgba(0,0,0,0.7)` 
        });
      } catch (error) {
        console.error("Error submitting form:", error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Error adding land inspector. Please try again.',
          backdrop: `rgba(0,0,0,0.7)`
        });
      }
    } else {
      Swal.fire({
        icon: 'error',
        title: 'MetaMask Not Installed',
        text: 'Please install MetaMask to interact with the Ethereum network.',
        backdrop: `rgba(0,0,0,0.7)` 
      });
    }

    setLoading(false);
  };

  return (
    <div>
      <div className="side-bar-header">
        <h2>Contract Owner Dashboard</h2>
      </div>
      <Sidebar />
      <div className="add-inspector">
        {loading && <div className="loader-overlay"><div className="loader"></div></div>} 
        <form className="add-inspector-form" onSubmit={handleSubmit}>
          <h2>Add Land Inspector</h2>
          <label>Full Name:</label>
          <input
            type="text"
            placeholder="Enter Inspector's Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
          <label>Wallet Address:</label>
          <input
            type="text"
            placeholder="Enter Inspector's Wallet Address"
            value={walletAddress}
            onChange={(e) => setWalletAddress(e.target.value)}
            required
          />
          <label>Email:</label>
          <input
            type="email"
            placeholder="Enter Inspector's Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <button type="submit">Add Land Inspector</button>
        </form>
      </div>
    </div>
  );
}

export default AddLandInspector;
