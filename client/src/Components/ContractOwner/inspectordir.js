import React, { useState, useEffect } from "react";
import Sidebar from "./sidebar";
import Modal from "./Modal";
import "./css/inspectordir.css";
import Swal from 'sweetalert2';
import Web3 from "web3";
import { LandChain_ABI, LandChain } from "../../abi/landChain";

function Loader() {
    return (
        <div className="loader-overlay">
            <div className="loader"></div>
        </div>
    );
}

function InspectorDirectory() {
    const [inspectors, setInspectors] = useState([]);
    const [isUpdating, setIsUpdating] = useState(false);
    const [currentInspector, setCurrentInspector] = useState({ name: "", email: "", walletAddr: "" });
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const fetchInspectors = async () => {
            if (window.ethereum) {
                const web3 = new Web3(window.ethereum);
                try {
                    setLoading(true); 
                    await window.ethereum.enable();
                    const accounts = await web3.eth.getAccounts();
                    const account = accounts[0];
                    const landChainContract = new web3.eth.Contract(
                        LandChain_ABI,
                        LandChain
                    );
                    const inspectorList = await landChainContract.methods
                        .getAllInspectors()
                        .call({ from: account });
                    setInspectors([...inspectorList].reverse()); // Create a shallow copy and reverse
                } catch (error) {
                    console.error("Error fetching inspectors:", error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Error fetching inspectors. Please try again later.',
                        backdrop: `rgba(0,0,0,0.7)`
                    });
                } finally {
                    setLoading(false); 
                }
            } else {
                console.error("MetaMask is not installed.");
                Swal.fire({
                    icon: 'error',
                    title: 'MetaMask Not Installed',
                    text: 'Please install MetaMask to interact with the Ethereum network.',
                    backdrop: `rgba(0,0,0,0.7)`
                });
            }
        };

        fetchInspectors();
    }, []);

    const deleteInspector = async (walletAddr) => {
        if (window.ethereum) {
            const web3 = new Web3(window.ethereum);
            try {
                setLoading(true); 
                await window.ethereum.enable();
                const accounts = await web3.eth.getAccounts();
                const account = accounts[0];
                const landChainContract = new web3.eth.Contract(
                    LandChain_ABI,
                    LandChain
                );
                await landChainContract.methods
                    .removeInspector(walletAddr)
                    .send({ from: account });
                setInspectors([...inspectors.filter(inspector => inspector.walletAddr !== walletAddr)].reverse()); // Create a shallow copy and reverse
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    text: 'Inspector deleted successfully!',
                    backdrop: `rgba(0,0,0,0.7)`
                });
            } catch (error) {
                console.error("Error deleting inspector:", error);
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error deleting inspector. Please try again.',
                    backdrop: `rgba(0,0,0,0.7)`
                });
            } finally {
                setLoading(false);
            }
        } else {
            console.error("MetaMask is not installed.");
            Swal.fire({
                icon: 'error',
                title: 'MetaMask Not Installed',
                text: 'Please install MetaMask to interact with the Ethereum network.',
                backdrop: `rgba(0,0,0,0.7)`
            });
        }
    };

    const startUpdate = (inspector) => {
        setCurrentInspector(inspector);
        setIsUpdating(true);
    };

    const updateInspector = async (event) => {
        event.preventDefault();
        if (window.ethereum) {
            const web3 = new Web3(window.ethereum);
            try {
                setLoading(true);
                await window.ethereum.enable();
                const accounts = await web3.eth.getAccounts();
                const account = accounts[0];
                const landChainContract = new web3.eth.Contract(
                    LandChain_ABI,
                    LandChain
                );
                await landChainContract.methods
                    .updateInspector(currentInspector.walletAddr, currentInspector.name, currentInspector.email)
                    .send({ from: account });
                setInspectors([...inspectors.map(inspector =>
                    inspector.walletAddr === currentInspector.walletAddr ? currentInspector : inspector
                )].reverse()); // Create a shallow copy and reverse
                setIsUpdating(false);
                setCurrentInspector({ name: "", email: "", walletAddr: "" });
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    text: 'Inspector updated successfully!',
                    backdrop: `rgba(0,0,0,0.7)`
                });
            } catch (error) {
                console.error("Error updating inspector:", error);
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error updating inspector. Please try again.',
                    backdrop: `rgba(0,0,0,0.7)`
                });
            } finally {
                setLoading(false);
            }
        } else {
            console.error("MetaMask is not installed.");
            Swal.fire({
                icon: 'error',
                title: 'MetaMask Not Installed',
                text: 'Please install MetaMask to interact with the Ethereum network.',
                backdrop: `rgba(0,0,0,0.7)`
            });
        }
    };

    return (
        <div>
            {loading && <Loader />}
            <div className='side-bar-header'>
                <h2>Contract Owner Dashboard</h2>
            </div>
            <Sidebar />

            <div className="inspector-table-container">
                <div className="stats">
                    <h2>Yearly Performance</h2>
                    <h2>Total Land Inspectors: {inspectors.length}</h2>
                </div>
                <h2>Land Inspectors</h2>
                <div className="table-wrapper">
                    <table className="inspector-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Wallet Address</th>
                                <th>Email</th>
                                <th>Vote Count</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {inspectors.map((inspector, index) => (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{inspector.name}</td>
                                    <td>{inspector.walletAddr}</td>
                                    <td>{inspector.email}</td>
                                    <td>{inspector.voteCount}</td>
                                    <td>
                                        <button
                                            className="action-button update-button"
                                            onClick={() => startUpdate(inspector)}
                                        >
                                            Update
                                        </button>
                                        <button
                                            className="action-button delete-button"
                                            onClick={() => deleteInspector(inspector.walletAddr)}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>

                <Modal isOpen={isUpdating} onClose={() => setIsUpdating(false)}>
                    <form onSubmit={updateInspector} className="update-form">
                        <h2>Update Inspector</h2>
                        <label>
                            Name:
                            <input
                                type="text"
                                value={currentInspector.name}
                                onChange={(e) => setCurrentInspector({ ...currentInspector, name: e.target.value })}
                            />
                        </label>
                        <label>
                            Email:
                            <input
                                type="email"
                                value={currentInspector.email}
                                onChange={(e) => setCurrentInspector({ ...currentInspector, email: e.target.value })}
                            />
                        </label>
                        <button type="submit" className="action-button save-button">Save</button>
                        <button type="button" className="action-button cancel-button" onClick={() => setIsUpdating(false)}>Cancel</button>
                    </form>
                </Modal>
            </div>
        </div>
    );
}

export default InspectorDirectory;
