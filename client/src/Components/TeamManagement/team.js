import React, { useEffect } from "react";
import { Icon } from "@iconify/react";
import Navigation from "../Landing/Navigation";
import Footer from "../Landing/footer";
import logo from './image/footerlogo.png';
import Organogram from "../TeamOrganogram/organogram";
import AOS from 'aos';
import 'aos/dist/aos.css';
import "./team.css";

const Team = () => {
    useEffect(() => {
        AOS.init({
            duration: 1000,
            easing: 'ease-in-out'
        });
    }, []);

    return (
        <div className="team_hero">
            <Navigation />
            <div className='logo' data-aos="fade-right">
                <img src={logo} className="logo" alt='logo' height={50} />
            </div>

            <div className='team_text' data-aos="fade-left">
                <p>
                    "Team management is about unlocking the full potential of each individual, while
                    harnessing the collective strength of the team to achieve extraordinary results".
                </p>
            </div>
            <div className='team_search' data-aos="fade-up">
                <input placeholder="search..." type="text"></input>
                <button>
                    <Icon icon="bi:arrow-right" className="right-arrow" />
                </button>
            </div>
            <br></br><br></br>
            <br></br><br></br>


            <h2 data-aos="fade-up">Meet the Team</h2>
            <div className="team-members" data-aos="fade-up">
                <div className="members">
                    <img src="https://previews.dropbox.com/p/thumb/ACT0FQtQW9-W8XoS8FXNP3Lh1kyep8wXaCs1TGn92kN_t90NGtmHnU11okCUXFUsE9E5K7X5fa1xcTazLszbS51_rvljhVM0zm9k9edW7b-Pno8WJJh1ABrZh7cwJUhyxrP558-vUhKGHXq_2dY8Tcy1Mc5NLvRI01NBJieSmzOjGTm4o-xY0c2y3eVZsfsHx77fr6DTWd11h_fbi7GTsiO5828C1yF5DPUi_Enru8fHYNnFKzlaLAue6l5VrOhKASZ4_KcB0QcIs9k8dfyQL0zHkf4LTQE3PBN2-Rtk9bGma0ymeuxuMo9xxh6x1gXxNUn_QKwEPT-27mNfe1lgnaSc/p.jpeg" alt="#" />
                    <p>Jigme Tenzin</p>
                    <h3>Founder</h3>
                </div>
                <div className="members">
                    <img src="https://previews.dropbox.com/p/thumb/ACSsclXB1uxUD8l7NFhzFvuIRM7bvFJxRMn451G17Cyhy6sS1HIguy6q7s0b4tzW6MV_ixlByGA10gqBfYENfrCD1eP-yYOwpHxUKy1S6Nd4WaKrXlbqUbO38HsOzLuy_V1w4zmuf1w-MAuT5s54AB5UQfqUgvEFx036B3CY3HMhVAP2-kVSbH62eIebvG0ME0ufaoX4ZFYm4QUwJcZH12fMu7Syl7920AQGaKtRE0ylw89Bl2sjEWdZ1iORDpRBz4oVwQnBtraH1MPebXvBPMTlqexn1ZnqY6oxtlP4cTkh_481O5fW13MPFcqtkNja99URGdMThD0UJOWeFIjcBLY6/p.jpeg" alt="#" />
                    <p>Chimi Rinzin</p>
                    <h3>Co Founder</h3>
                </div>
                <div className="members">
                    <img src="https://previews.dropbox.com/p/thumb/ACRvYtomh2pbpdYuwIpr7V7oY5Lg5YuoILzxdJGQSDD8jFpoh4sLqpiJRnDY86WqZVy_TwViarRNupIwu7OR1TmKzeGSbpFL_fJicWnOxlgmYKON2ncQf3EYXtJoBqGkIg0Ug8uHZieB8pui08HCM7sQkavYoScrn1lFfbHQkuuBYdr92Qn5asWNPjAnCZrbeQOxPBzI-JCEMYHpGQQjQPvN9PHJK1r8EtJM00Kzvn7BGtLa86Dn3cV0TWLmK3y1Ngv3zCV582oyi4hDGQXN12b2a__9m_nELueZgc4Crun14Ipm_TJfAqUEN6ED7EvIX9I4CHHscccZL1r_ZxqfIjnI/p.jpeg" alt="#" />
                    <p>Deepak Ghalley</p>
                    <h3>Chief Executive Officer</h3>
                </div>
                <div className="members">
                    <img src="https://media-sin6-4.cdn.whatsapp.net/v/t61.24694-24/436995294_984699803125492_5023953051432465046_n.jpg?ccb=11-4&oh=01_Q5AaIOvRNSMHGkIchHSWB_ab8CdDjNEa3iq-wbCVsvuPVN11&oe=66660923&_nc_sid=e6ed6c&_nc_cat=103" alt="#" />
                    <p>Tshering Dorji</p>
                    <h3>Chief Operating Officer</h3>
                </div>
                <div className="members">
                    <img src="https://media-ccu1-2.cdn.whatsapp.net/v/t61.24694-24/414464545_829819265142573_3579883637138544375_n.jpg?ccb=11-4&oh=01_Q5AaICgc1gK0QSflquRe_m_oO9dic38fj6gcwt7J3ekBCjJN&oe=6669D02D&_nc_sid=e6ed6c&_nc_cat=100" alt="#" />
                    <p>Dorji Wangmo</p>
                    <h3>Chief Financial Officer</h3>
                </div>
                <div className="members">
                    <img src="https://media-sin6-4.cdn.whatsapp.net/v/t61.24694-24/422814143_354428160584464_309670316374483731_n.jpg?ccb=11-4&oh=01_Q5AaICLROBrQwqlk1n6cBDKzR-UZNrPYiW5tA1SdF-IB3n0V&oe=666678FF&_nc_sid=e6ed6c&_nc_cat=109" alt="#" />
                    <p>Choki Lhamo</p>
                    <h3>Manager</h3>
                </div>

            </div>
            <div className="why-work-with-us" data-aos="fade-up">                <div className="team-left">
                <h2>Why Work With Us?</h2>
                <p>Thinking of a career with DrukLandChain?</p>
                <img src="https://previews.dropbox.com/p/thumb/ACR6ODeASy9XpXsxIPb_RNfKpYU9Ji3AetzPjtKVpB2Yg8qZJEBJgKxj87ztcmp7iO8KFnVLI-P97E_mzfQdV9Uk3TaFpQT4lu7tcwphUQ4TUXJnBhdCAbzP3GXz_yMZqo5PPUI4XJJ-p42M30XiHJq0OlUBdBDuX4-Gre3ePzth586BzfVZkj7n50ZymgiqTeaDO2mexP89FNblGB19TWmGRoH06VSN0JRzqKGyINlUGm-ygrdZbtEW3J2h2ct7LbbYIg6xKEYAf07RvDsobk31BdYK-wDYaLI2qCH7CREUAg79hXyEliuSXLJD9oeVEjETQ__4EYM5AW8zIYux4sVG/p.png" alt="#" />

            </div>
                <div className="team-right">
                    <p>
                        "At DrukLandChain, we understand the complexities and importance of land transactions. Our platform offers a
                        comprehensive suite of services tailored to meet the needs of both land buyers and sellers. With our user-friendly
                        interface and cutting-edge technology, we strive to simplify the process of buying and selling land, making it efficient,
                        transparent, and secure. Whether you're a seasoned investor, a first-time buyer, or a property owner looking to sell, our team of
                        experts is dedicated to providing personalized support and guidance every step of the way. Trust DrukLandChain to help you navigate
                        the intricacies of land transactions and unlock the opportunities that await in the real estate market."
                    </p>
                </div>
            </div>
            <div className="team-Organogram" data-aos="fade-up">
                <Organogram />
            </div>
            <div className="team-footer" data-aos="fade-up">
                <Footer />
            </div>

        </div>
    )
}
export default Team;