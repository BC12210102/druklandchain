import React from "react";
import { Link } from 'react-router-dom';
import './CSS/Role.css'
import { roleData } from './Js/role';

const Role = () => {
    return (
        <div className="role">
            <div className="options">
                <div className="contract-owner">
                    <div className="image-contract">
                        <img className="service_img" src={roleData[0].image_1} alt="Transfer" />
                    </div>
                    <Link to="/contract-owner">
                        <button className="role">Contract Owner</button>
                    </Link>
                </div>
                <div className="land-inspector">
                    <div className="image-inspector">
                        <img className="service_img" src={roleData[1].image_2} alt="Transfer" />
                    </div>
                    <Link to="/land-inspector">
                        <button className="role">Land Inspector</button>
                    </Link>
                </div>
                <div className="users">
                    <div className="image-user">
                        <img className="service_img" src={roleData[2].image_3} alt="Transfer" />
                    </div>
                    <Link to="/land-deals">
                        <button className="role">Land Deals & Offers</button>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default Role;
