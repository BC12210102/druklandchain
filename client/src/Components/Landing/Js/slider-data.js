export const sliderData = [
    {
        image: "https://cdr-international.nl/wp-content/uploads/2022/02/01.-CDR-Bhutan-RVO-project-river-maw.jpg",
        heading: "Land prices in Gelephu to zoom up due to growing interest",
        desc: "As the land moratorium in Gelephu continues, both the people and the real estate agents are expecting a significant rise in land prices once the restrictions on the sale and purchase of the lands are lifted.",
    },
    {
        image: "https://kuenselonline.com/wp-content/uploads/2024/04/Balujora-and-Pasakha.jpg",
        heading: "On state land lease rates",
        desc: " group of industrialists based in Balujora within the Pasakha LAP has voiced concerns over the newly revised state land lease rates, petitioning the Phuentsholing Thromde office for a 10-year deferment."
    },
    {
        image: "https://kuenselonline.com/wp-content/uploads/2024/04/Pachu4.jpg",
        heading: "Government Announces New Land Regulations",
        desc: "The government has introduced new regulations aimed at streamlining land transactions and improving transparency in the real estate sector. Learn more about the key changes and their impact on land buyers and sellers.",
    },
    {
        image: "https://kuenselonline.com/wp-content/uploads/2023/12/memorandum.jpg",
        heading: "Collaboration to boost the technological proficiency of NLCS",
        desc: "tarting January 2024, about 230 staff of the National Land Commission Secretariat (NLC) will be trained on relevant technological needs of the agency through tailored training in partnership with the Gyalpoizhing College of Information technology (GCIT).",
    },
    {
        image: "https://kuenselonline.com/wp-content/uploads/2024/04/Lyonchhen.jpg",
        heading: "Systems up to stop multiple land mortgaging ",
        desc: "A person cannot mortgage the same plot of land multiple times with different financial institutions hereafter after the National Land Commission’s (NLC) e-sakor, a land information service portal is integrated with the land mortgage system (LMS).",
    },
    {
        image: "https://www.bbs.bt/news/wp-content/uploads/2019/01/HRH-gyaltshab-launches-NLC.jpg",
        heading: "Making land service delivery efficient",
        desc: "NLC Secretariat’s secretary, Pema Chewang, said land is an indispensable factor of production and most basic for socio-economic development. “It is one of the primary sources of wellbeing and happiness. Existence of human beings is utterly impossible without land.",
    },


];