import React, { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import Navigation from './Navigation';
import './CSS/Home.css';
import { Icon } from "@iconify/react";
import logo from './image/footerlogo.png';
import Slider from './Slider';
import Role from './Role';
import Services from './Services';
import Agent from './Agent';
import GetInTouch from './GetInTouch';
import Footer from './footer';

function Home() {
    useEffect(() => {
        AOS.init({
            duration: 1000
        });
    }, []);

    return (
        <div className='home_hero'>
            <Navigation />
            <div className='logo' data-aos="fade-down">
                <img src={logo} className="logo" alt='logo' height={50} />
            </div>
            <div className='home_text' data-aos="fade-up">
                <p>
                    “Secure Your Property Rights Today Register Your Land Records
                    Hassle-Free Gain Peace of Mind with Our Trusted Land Registry System”
                </p>
            </div>
            <div className='home_search' data-aos="fade-right">
                <select>
                    <option>Land Type</option>
                    <option>Urban</option>
                    <option>Rural</option>
                </select>
                <select>
                    <option>Location</option>
                    <option>Urban</option>
                    <option>Rural</option>
                </select>
                <select>
                    <option>Price Range</option>
                    <option>Urban</option>
                    <option>Rural</option>
                </select>
                <button>
                    <Icon icon="bi:arrow-right" className="right-arrow" />
                </button>
            </div>
            <div className='latestNews' data-aos="fade-left">
                <h2>Latest News & Article</h2>
                <Slider />
            </div>
            <div className='select-role' data-aos="fade-up">
                <h2>Select Your Role</h2>
                <Role />
            </div>
            <div className='services' data-aos="fade-up">
                <h2>Discover Our Services</h2>
                <Services />
            </div>
            <div className='featured_agent' data-aos="fade-up">
                <Agent />
            </div>
            <div className="get-in-touch" data-aos="fade-up">
                <GetInTouch />
            </div>
            <div className='footer-home' data-aos="fade-up">
                <Footer />
            </div>
        </div>
    )
}

export default Home;
