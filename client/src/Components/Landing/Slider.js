import React, { useState } from "react";
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai";
import { sliderData } from "./Js/slider-data";
import "./CSS/slider.css";

const Slider = () => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const slideLength = sliderData.length;

  const nextSlide = () => {
    setCurrentSlide((currentSlide + 1) % slideLength);
  };

  const prevSlide = () => {
    setCurrentSlide((currentSlide - 1 + slideLength) % slideLength);
  };

  return (
    <div className="slider">
      <AiOutlineArrowLeft
        className="arrow prev"
        onClick={prevSlide}
      />
      <AiOutlineArrowRight
        className="arrow next"
        onClick={nextSlide}
      />
      {sliderData.map((slide, index) => (
        <div
          className={index === currentSlide ? "slide current" : "slide"}
          key={index}
        >
          {index === currentSlide && (
            <div>
              <img
                src={slide.image}
                alt="slide"
                className="image"
              />
              <div className="content">
                <h2>{slide.heading}</h2>
                <p>{slide.desc}</p>
                <button className="news-btn">
                  Read More
                </button>
              </div>
            </div>
          )}
        </div>
      ))}
    </div>
  );
};

export default Slider;
