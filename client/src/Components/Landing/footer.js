import React from 'react';
import './CSS/footer.css';
import { Icon } from "@iconify/react";

import footerlogo from './image/footerlogo.png';

const Footer = () => {
  return (
    <div>
      <div className='footer'>
        <div className='footer-div1'>
          <div className='footer-logo'>
            <img src={footerlogo} alt="Footer Logo" height={50} />
          </div>
          <div className='footer-sub'>
            <h2>Get Notification</h2>
            <div className='footer_submit'>
              <input type='text' placeholder='Enter Your Email'></input>
              <button>
                <Icon icon="bi:arrow-right" className="right-arrow" />
              </button>
            </div>
            <div className='social-media-footer'>
              <div className='git'>
                <img src='https://cdn-icons-png.freepik.com/256/14772/14772455.png' alt='#' height={50}></img>
              </div>
              <div className='face'>
                <img src='https://cdn.icon-icons.com/icons2/2985/PNG/512/facebook_social_media_icon_187125.png'alt='#' height={50}></img>
              </div>
              <div className='link'>
                <img src='https://static-00.iconduck.com/assets.00/linkedin-icon-2048x2048-7zb6wa8q.png' alt='#'height={50}></img>
              </div>
              <div className='insta'>
                <img src='https://i.pinimg.com/564x/43/d8/28/43d8288a17c02c2ab174dcdba62117e9.jpg'alt='#' height={50}></img>
              </div>
            </div>
          </div>
        </div>
        <div className='footer-div2'>
          <h2>About Us</h2>
          <ul>
            <li>Location</li>
            <li>Why us</li>
            <li>services</li>

          </ul>
        </div>
        <div className='footer-div3'>
          <h2>Contact Us</h2>
          <ul>
            <li>Email:druklandchain@gmail.bt</li>
            <li>Phone:+975 17952687</li>
            <li>Address: Kabjesa,Thimphu,Bhutan</li>
          </ul>
        </div>
        <div className='footer-div4'>
        <h2>Team Management</h2>
          <ul>
            <li>Management</li>
            <li>Overview</li>
          </ul>
        </div>
      </div>
      <div className='footer-foot'>
        <p>© 2024 DrukLandChain, Bhutan</p>
      </div>
    </div>
  );
}

export default Footer;
