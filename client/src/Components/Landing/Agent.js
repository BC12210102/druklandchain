import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import './CSS/Agent.css';

const Agent = () => {
    const agentSliderSettings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
    };

    const agents = [
        { id: 1, name: "Choki Lhamo", location: "Thimphu, Bhutan", image: "https://media-sin6-4.cdn.whatsapp.net/v/t61.24694-24/422814143_354428160584464_309670316374483731_n.jpg?ccb=11-4&oh=01_Q5AaICLROBrQwqlk1n6cBDKzR-UZNrPYiW5tA1SdF-IB3n0V&oe=666678FF&_nc_sid=e6ed6c&_nc_cat=109" },
        { id: 2, name: "Dorji Wangmo", location: "Thimphu, Bhutan", image: "https://previews.dropbox.com/p/thumb/ACRbtVGQ8_Gfvn5CTeDLzCcwT6FzHzlm_1Bem3VVJme618MrlsfgOiJ570zFMTXYJA5z_S5Zqy8qxxMJC-ZD5EdlBfQ-TaeVWRdMvVwPpzTb0q_1AXgNCa5g5nHxK8JMSU8gTaILHXIV3GajylUPLgP5_iovPdUwEY2eU-wLhR7GjxQlT7edbnDYcV_vNosAeRkN75tld4YaG1RoPoQmuYRqfvWHadEuz6HaZ6MilDCuRyD9OlysCIYAVM_nmzUqst2QFSCBUTt83k3NtuFuhkk0pXOyh37E4zYe2n2pZkabQgpz_zwlBjMw3UYNyqkAG4aSi3CTBj56RVGpt_o6SIuay3ZcA5LilIAgG2kJ0VLH9A/p.jpeg" },
        { id: 3, name: "Tshering Dorji", location: "Thimphu, Bhutan", image: "https://media-sin6-4.cdn.whatsapp.net/v/t61.24694-24/436995294_984699803125492_5023953051432465046_n.jpg?ccb=11-4&oh=01_Q5AaIOvRNSMHGkIchHSWB_ab8CdDjNEa3iq-wbCVsvuPVN11&oe=66660923&_nc_sid=e6ed6c&_nc_cat=103" },
        { id: 4, name: "Drukden Dorji Wangchuk", location: "Thimphu, Bhutan", image: "https://media-sin6-4.cdn.whatsapp.net/v/t61.24694-24/321092888_707073174369009_2031144949152344986_n.jpg?ccb=11-4&oh=01_Q5AaIHZr9u5eXJrM3uKVnaYnEwaJMxFjEvp2Eyzsw1nGnbbm&oe=6665F732&_nc_sid=e6ed6c&_nc_cat=103" },
    ];
    const socailMedia = [
        { image: "https://cdn.icon-icons.com/icons2/2985/PNG/512/facebook_social_media_icon_187125.png" },
        { image: "https://cdn-icons-png.freepik.com/256/14772/14772455.png" },
        { image: "https://static-00.iconduck.com/assets.00/linkedin-icon-2048x2048-7zb6wa8q.png" },
        { image: "https://i.pinimg.com/564x/43/d8/28/43d8288a17c02c2ab174dcdba62117e9.jpg" }
    ];

    return (
        <div>
            <div className="agent_slider">
                <h2>Featured Agent</h2>
                <Slider {...agentSliderSettings}>
                    {agents.map((agent) => (
                        <div key={agent.id} className="agent">
                            <img src={agent.image} alt={agent.name} />
                            <h3>{agent.location}</h3>
                            <h4>{agent.name}</h4>
                            <div className="social-media">
                                {socailMedia.map((media, index) => (
                                    <div key={index} className="social-media-item">
                                        <img src={media.image} alt={`social-media-${index}`} />
                                    </div>
                                ))}
                            </div>
                            <div className="agent-button">
                                <button>Message</button>
                                <button> Call</button>
                            </div>

                        </div>

                    ))}
                </Slider>
            </div >
        </div >
    );
};

export default Agent;
