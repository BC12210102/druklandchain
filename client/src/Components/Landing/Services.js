import React from 'react';
import './CSS/Services.css';
import { imageData } from './Js/image';

const Services = () => {
    return (
        <div className="main_div">
            <div className='service-div'>
                <div className='transfer'>
                    <div className='circle'>
                        <img className="service_img"src={imageData[0].image_1} alt="Transfer" />
                    </div>
                    <h5>Land Ownership Transfer</h5><br></br>
                    <p> Land transfer of ownership provides decentralized decision-making, governance, and execution.</p>
                </div>
                <div className='payments'>
                    <div className='circle_1'>
                        <img className="service_img"src={imageData[1].image_2} alt="Transfer" />
                    </div>
                    <h5>Payments</h5><br></br>
                    <p>Transactions occur through the use of cryptocurrency and Druk Token(DT).</p>
                </div>
                <div className='land-record'>
                    <div className='circle'>
                        <img className="service_img"src={imageData[2].image_3} alt="Transfer" />
                    </div>
                    <h5>Land Records</h5><br></br>
                    <p>Land records offers numerous benefits, including increased transparency, immutability, and security.</p>
                </div>
                <div className='token-exchange'>
                    <div className='circle_1'>
                        <img className="service_img"src={imageData[3].image_4} alt="Transfer" />
                    </div>
                    <h5>Token Exchange</h5><br></br>
                    <p>Token exchange within Decentralized Autonomous Organization (DAO) involves the trading or swapping of digital assets.</p>
                </div>
                <div className='register-land'>
                    <div className='circle'>
                        <img className="service_img"src={imageData[4].image_5} alt="Transfer" />
                    </div>
                    <h5>Land Info Registry</h5><br></br>
                    <p>Serves as a decentralized and transparent repository for recording and managing land-related information on a blockchain network.</p>
                </div>
                <div className='market-platform'>
                    <div className='circle_1'>
                        <img className="service_img"src={imageData[5].image_6} alt="Transfer" />
                    </div>
                    <h5>Marketplace</h5><br></br>
                    <p>It facilitates peer-to-peer transactions without the need for intermediaries, providing a transparent and efficient marketplace.</p>
                </div>
            </div>
        </div>
    );
};

export default Services;
