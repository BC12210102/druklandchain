import React from 'react';
import './CSS/GetInTouch.css'
const GetInTouch = () => {
    return (
        <div>
            <div className='getInTouch'>
                <div className='left'>
                    <h2 className='h2'>Let's get in touch</h2>
                    <p className='p1'>We're open for any suggestion or to have a chat</p>
                    <div className='circle_getintouch'>
                        <img src='https://static.vecteezy.com/system/resources/previews/000/599/083/original/location-icon-vector.jpg' alt='location' height={40}></img>
                    </div>
                    <div className='circle_getintouch'>
                        <img src='https://www.iconpacks.net/icons/1/free-phone-icon-1-thumb.png' alt='phone' height={30}></img>
                    </div>
                    <div className='circle_getintouch'>
                        <img src='https://icon-library.com/images/send-message-icon/send-message-icon-22.jpg' alt='message' height={50}></img>
                    </div>
                    <div className='info-div'>
                        <p>Address: Kabjesa,Thimphu,Bhutan</p>
                        <p>Phone:+975 17952687</p>
                        <p>Email:druklandchain@gmail.bt</p>

                    </div>
                    <div className='social-media-getintouch'>
                        <div className='git'>
                            <img src='https://cdn-icons-png.freepik.com/256/14772/14772455.png' alt="git hub" height={50}></img>
                        </div>
                        <div className='face'>
                            <img src='https://cdn.icon-icons.com/icons2/2985/PNG/512/facebook_social_media_icon_187125.png' alt='facebook' height={50}></img>
                        </div>
                        <div className='link'>
                            <img src='https://static-00.iconduck.com/assets.00/linkedin-icon-2048x2048-7zb6wa8q.png' alt='linkedin'height={50}></img>
                        </div>
                        <div className='insta'>
                            <img src='https://i.pinimg.com/564x/43/d8/28/43d8288a17c02c2ab174dcdba62117e9.jpg' alt='instagram'height={50}></img>
                        </div>
                    </div>
                </div>
                <div className='right'>
                    <h2>Get in touch</h2>
                    <form className='get-in-touch-form'>
                        <div>
                            <label className='get-in-touch-label'>Full Name</label><br></br><br></br>
                            <input type='name' placeholder='Enter Your Name'></input>
                        </div>
                        <div>
                            <label className='get-in-touch-label'>Email</label><br></br><br></br>
                            <input type='email' placeholder='Enter Your Email'></input>
                        </div>
                        <div>
                            <label className='get-in-touch-label'>Subject</label><br></br><br></br>
                            <input type='subject' placeholder='Enter Your Subject'></input>
                        </div>
                        <div>
                            <label className='get-in-touch-label'>Contact Number</label><br></br><br></br>
                            <input type='tel' placeholder='Enter Your Contract Number'></input>
                        </div>
                        <div>
                            <div className="text-inputs">
                                <label className='get-in-touch-label'>Message</label><br></br><br></br>
                                <input type='text' placeholder='Message....'></input>
                            </div>
                        </div>
                    </form>
                    <button className='btn-message'>Send Message</button>
                </div>

            </div>

        </div>
    )

}
export default GetInTouch;