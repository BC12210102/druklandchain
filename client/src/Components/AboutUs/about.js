import React, { useEffect } from "react";
import { Icon } from "@iconify/react";
import Navigation from "../Landing/Navigation";
import logo from './image/footerlogo.png';
import "./about.css";
import Footer from "../Landing/footer";
import AOS from 'aos';
import 'aos/dist/aos.css';

const About = () => {
    useEffect(() => {
        AOS.init({
            duration: 1000, // Duration of the animation
            easing: 'ease-in-out', // Easing option
            once: true // Animation only occurs once
        });
    }, []);

    return (
        <div className='about_hero'>
            <Navigation />
            <div className='logo' data-aos="fade-right">
                <img src={logo} className="logo" alt='logo' height={50} />
            </div>

            <div className='about_text' data-aos="fade-left">
                <p>
                    “At DrukLandChain, we believe in the transformative power of land ownership.”
                </p>
            </div>
            <div className='about_search' data-aos="fade-up">
                <input placeholder="search..." type="text"></input>
                <button>
                    <Icon icon="bi:arrow-right" className="right-arrow" />
                </button>
            </div>
            <div className="about-us-main">
                <h2>Introduction</h2>

                <div className="about-us-intro" data-aos="fade-right">
                    <img src="https://i.pinimg.com/564x/52/ff/3d/52ff3d2f76ed23d3cc473afc115dfc66.jpg" alt="#" />
                    <p>
                        Welcome to DrukLandChain, where land transactions meet innovation. At DrukLandChain, our mission
                        is to revolutionize the way land transactions are conducted, making them more accessible, transparent,
                        and secure for everyone involved. We believe that by leveraging cutting-edge technology and a commitment to
                        excellence, we can empower individuals and communities to unlock the potential of their land assets.
                    </p>
                </div>
                <h2>Mission and Values</h2>
                <div className="about-us-mission" data-aos="fade-left">
                    <p>
                        Our mission at DrukLandChain is simple: to provide a trusted platform for land transactions that promotes fairness,
                        efficiency, and integrity. We are driven by a set of core values that guide our every action: transparency, accountability, innovation,
                        and customer-centricity. These values serve as the foundation of our organization and shape everything we do.
                    </p>
                    <img src="https://i.pinimg.com/564x/23/36/ad/2336ad3d8917fe43997a53c330f13e82.jpg" alt="#" />

                </div>
                <h2>History and Background</h2>

                <div className="about-us-background" data-aos="fade-right">
                    <img src="https://i.pinimg.com/564x/a6/09/c9/a609c92e30d75a2e83ee5a70a472301a.jpg" alt="#" />
                    <p>
                        Founded in 2024, DrukLandChain has quickly emerged as a leader in the land transaction industry.
                        Over the years, we have achieved significant milestones and garnered
                        recognition for our commitment to excellence and innovation.
                        From our humble beginnings to our current status as a trusted
                        partner for land buyers and sellers, our journey has been marked
                        by dedication, perseverance, and a relentless pursuit of excellence.
                    </p>
                </div>
            </div>
            <div className="about-footer" data-aos="fade-up">
                <Footer />
            </div>
        </div>
    );
};

export default About;
