import React, { useState, useEffect } from "react";
import { Link as RouterLink } from "react-router-dom"; 
import logo from "./image/logo.png";
import "./css/inspectorLogin.css";
import metamaskImage from "./image/metamask.png";
import smallImage from "./image/metamask.png";
import Swal from 'sweetalert2';
import VerifyLand from "./verify";
import AOS from 'aos';
import 'aos/dist/aos.css';

function InspectorLogin() {
  const [loading, setLoading] = useState(false);
  const [loggedIn, setLoggedIn] = useState(false);
  
  useEffect(() => {
    AOS.init({
      duration: 1000,
      easing: 'ease-in-out', 
      once: true 
    });
  }, []);

  const handleConnectMetamask = async () => {
    try {
      setLoading(true);
      await window.ethereum.request({ method: 'eth_requestAccounts' });
      setLoading(false);
      Swal.fire({
        icon: 'success',
        title: 'Logged In',
        text: 'You have successfully logged in.',
        showConfirmButton: false,
        timer: 2000
      });
      setLoggedIn(true);
    } catch (error) {
      setLoading(false);
      console.error("Error connecting to Metamask:", error);
      Swal.fire({
        icon: 'error',
        title: 'Connection Failed',
        text: 'There was an error connecting to Metamask. Please try again.'
      });
    } finally {
      setLoading(false);
    }
  };

  if (loggedIn) {
    return <VerifyLand />;
  }

  return (
    <div className="container-box" data-aos="fade-up">
      <div className="logo-container">
        <img src={logo} alt="Your Logo" className="logo-inspector" />
        <p className="p">Welcome Land Inspector</p>
        <img src={metamaskImage} alt="Metamask" className="metamask-image" />
        <button onClick={handleConnectMetamask} className="connect-button">
          <img src={smallImage} alt="Small Image" className="small-image" />
          Connect to Metamask
        </button>
      </div>
      <RouterLink to="/" className="home-link">
        <button>Go Back</button>
      </RouterLink>
    </div>
  );
}

export default InspectorLogin;
