import React, { useState } from 'react';
import Swal from 'sweetalert2';
import "./css/inspector-sidebar.css";

const InspectorSidebar = () => {
  const [logoutConfirmed, setLogoutConfirmed] = useState(false);

  const handleLogout = (e) => {
    e.preventDefault(); 
    Swal.fire({
      icon: 'success',
      title: 'Logged Out',
      text: 'You have successfully logged out.',
      showConfirmButton: false,
      timer: 2000 
    }).then(() => {
      setLogoutConfirmed(true);
      window.location.href = "/";
    });
  };

  return (
    <div>
      <nav className='inspector-navigation'>
        <ul className='inspector-ul'>
          <li className='inspector-li'>
            <img src='https://i.pinimg.com/564x/e0/8e/57/e08e5711c93bcf905ef1ce94617f8828.jpg' alt='#' />
            <a href="/verify">Verify Land</a>
          </li>
          <li className='inspector-li'>
            <img src='https://i.pinimg.com/564x/42/92/e0/4292e0ea4b8e1fa3b969774d6b880314.jpg' alt='#' />
            {logoutConfirmed ? (
              <a href="/">Log Out</a>
            ) : (
              <a href="/" onClick={handleLogout}>Log Out</a>
            )}
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default InspectorSidebar;
