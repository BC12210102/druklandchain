import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Web3 from "web3";
import { LandChain_ABI, LandChain } from "../../abi/landChain";
import "./css/detail.css";
import Swal from 'sweetalert2';
import MapModal from "./MapModal"; 

function calculateLandPrice(totalAreaInSquareMeters) {
    const fixedPricePerSquareMeter = 0.01;
    return totalAreaInSquareMeters * fixedPricePerSquareMeter;
}

const formatWalletAddress = (address) => {
    if (!address) return '';
    return `${address.slice(0, 6)}...${address.slice(-4)}`;
};

const Loader = () => (
    <div className="loader-overlay">
        <div className="loader"></div>
    </div>
);

const DetailPage = () => {
    const { thramNumber } = useParams();
    const navigate = useNavigate();
    const [landInfo, setLandInfo] = useState(null);
    const [landImage, setLandImage] = useState(null);
    const [mapImage, setMapImage] = useState(null);
    const [showModal, setShowModal] = useState(false);
    const [loading, setLoading] = useState(true);
    const [isInspector, setIsInspector] = useState(false);
    const [connectedWallet, setConnectedWallet] = useState('');

    useEffect(() => {
        const fetchLandInfo = async () => {
            if (window.ethereum) {
                const web3 = new Web3(window.ethereum);

                try {
                    await window.ethereum.request({ method: 'eth_requestAccounts' });
                    const landChainContract = new web3.eth.Contract(LandChain_ABI, LandChain);
                    const landDetails = await landChainContract.methods.getLandDetailsByThram(parseInt(thramNumber)).call();
                    setLandInfo({
                        plotNumber: landDetails[0],
                        location: landDetails[1],
                        totalArea: landDetails[2],
                        owner: landDetails[3],
                        priceInEther: calculateLandPrice(landDetails[4]),
                        maphash: landDetails[5],
                        imagehash: landDetails[6],
                        verified: landDetails[7]
                    });

                    const accounts = await web3.eth.getAccounts();
                    const inspectorStatus = await landChainContract.methods.inspectors(accounts[0]).call();
                    setIsInspector(inspectorStatus);

                    setConnectedWallet(accounts[0]);

                    try {
                        const mapImageResponse = await fetch(`https://ipfs.io/ipfs/${landDetails[5]}`);
                        if (!mapImageResponse.ok) {
                            throw new Error('Failed to fetch map image');
                        }
                        const mapImageData = await mapImageResponse.blob();
                        setMapImage(URL.createObjectURL(mapImageData));
                    } catch (error) {
                        console.error("Error fetching map image: ", error);
                    }

                    try {
                        const landImageResponse = await fetch(`https://ipfs.io/ipfs/${landDetails[6]}`);
                        if (!landImageResponse.ok) {
                            throw new Error('Failed to fetch land image');
                        }
                        const landImageData = await landImageResponse.blob();
                        setLandImage(URL.createObjectURL(landImageData));
                    } catch (error) {
                        console.error("Error fetching land image: ", error);
                    }
                } catch (error) {
                    console.error("Error fetching land details: ", error);
                } finally {
                    setLoading(false);
                }
            } else {
                console.error("MetaMask is not installed.");
                setLoading(false);
            }
        };

        fetchLandInfo();
    }, [thramNumber]);

    const openModal = () => setShowModal(true);
    const closeModal = () => setShowModal(false);

    const verifyLand = async () => {
        if (!isInspector) {
            await Swal.fire({
                icon: 'warning',
                title: 'Warning',
                text: 'Only inspectors are allowed to verify land.',
            });
            return;
        }

        try {
            const web3 = new Web3(window.ethereum);
            await window.ethereum.request({ method: 'eth_requestAccounts' });
            const accounts = await web3.eth.getAccounts();
            const inspectorAddress = accounts[0];
            const landChainContract = new web3.eth.Contract(LandChain_ABI, LandChain);

            const thramNumberInt = parseInt(thramNumber);
            if (isNaN(thramNumberInt)) {
                throw new Error("Invalid Thram Number");
            }

            const landDetails = await landChainContract.methods.getLandDetailsByThram(thramNumberInt).call();
            if (!landDetails) {
                throw new Error("Land with the given Thram number does not exist.");
            }

            await landChainContract.methods.verifyLand(thramNumberInt).send({ from: inspectorAddress });
            setLandInfo({ ...landInfo, verified: true });
            await Swal.fire({
                icon: 'success',
                title: 'Success',
                text: 'Land verified successfully',
            });
        } catch (error) {
            await Swal.fire({
                icon: 'error',
                title: 'Error',
                text: error.message || 'Failed to verify land',
            });
            console.error("Error verifying land: ", error);
        }
    };

    const declineLand = async () => {
        if (!isInspector) {
            await Swal.fire({
                icon: 'warning',
                title: 'Warning',
                text: 'Only inspectors are allowed to decline land.',
            });
            return;
        }

        try {
            const web3 = new Web3(window.ethereum);
            await window.ethereum.request({ method: 'eth_requestAccounts' });
            const accounts = await web3.eth.getAccounts();
            const inspectorAddress = accounts[0];
            const landChainContract = new web3.eth.Contract(LandChain_ABI, LandChain);

            const thramNumberInt = parseInt(thramNumber);
            if (isNaN(thramNumberInt)) {
                throw new Error("Invalid Thram Number");
            }

            const landDetails = await landChainContract.methods.getLandDetailsByThram(thramNumberInt).call();
            if (!landDetails) {
                throw new Error("Land with the given Thram number does not exist.");
            }

            await landChainContract.methods.declineLand(thramNumberInt).send({ from: inspectorAddress });
            setLandInfo({ ...landInfo, verified: false });
            await Swal.fire({
                icon: 'success',
                title: 'Success',
                text: 'Land declined successfully',
            });
        } catch (error) {
            await Swal.fire({
                icon: 'error',
                title: 'Error',
                text: error.message || 'Failed to decline land',
            });
            console.error("Error declining land: ", error);
        }
    };

    const redirectToVerifyPage = () => {
        navigate('/verify');
    };

    if (loading) {
        return <Loader />;
    }

    return (
        <div className="detail-container">
            <button onClick={redirectToVerifyPage} className="go-to-verify-button">Back</button>
            <div className="profile-header">
                <img src="http://localhost:3000/static/media/footerlogo.20b6060e.png" alt="Logo" />
                <button>Log Out</button>
            </div>
            <div className="profile-hero">
                <img src="https://cdn.icon-icons.com/icons2/3624/PNG/512/verify_icon_227246.png" alt="Hero" />
                <div className="hero-text">
                    <p>“land information and the image of cadastral map and land"</p>
                </div>
            </div>
            <div className="wallet-detail">
                <p>Connected to: {formatWalletAddress(connectedWallet)}</p>
            </div>
            <div className="details-land">
                <div className="details-land">
                    <div className="detail-info">
                        <p className="detail-text">Thram Number: {thramNumber}</p>
                        {landInfo && (
                            <React.Fragment>
                                <p className="detail-text">Plot Number: {landInfo.plotNumber}</p>
                                <p className="detail-text">Location: {landInfo.location}</p>
                                <p className="detail-text">Total Area: {landInfo.totalArea}</p>
                                <p className="detail-text">Owner: {landInfo.owner}</p>
                                <p className="detail-text">Verified: {landInfo.verified ? "Yes" : "No"}</p>
                            </React.Fragment>
                        )}
                        {!landInfo.verified && (
                            <div className="action-buttons">
                                <button onClick={verifyLand} className="button verify-button" disabled={!isInspector}>
                                    Verify Land
                                </button>
                                <button onClick={declineLand} className="button-decline-button" disabled={!isInspector}>
                                    Decline Land
                                </button>
                            </div>
                        )}
                    </div>
                    <div className="detail-image">
                        {landImage && <img src={landImage} alt="Land" className="land-image" />}
                        <button onClick={openModal} className="button">View Map</button>
                    </div>
                </div>
            </div>

            {showModal && (
                <MapModal showModal={showModal} closeModal={closeModal} mapImage={mapImage} />
            )}
            <div className="profile-footer">
                <p>©2024 DrukLandChain, Bhutan</p>
            </div>
        </div>
        
    );
};

export default DetailPage;
