import React from "react";
import "./css/Mapmodal.css";

const MapModal = ({ mapImage, onClose }) => {
  return (
    <div className="modal-overlay" onClick={onClose}>
      <div className="modal-content">
        <span className="close" onClick={onClose}>&times;</span>
        <h2>Cadastral Map</h2>
        {mapImage && <img src={mapImage} alt="Map" />}
      </div>
    </div>
  );
};

export default MapModal;
