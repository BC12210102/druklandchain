import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Web3 from "web3";
import { LandChain_ABI, LandChain } from "../../abi/landChain";
import "./css/verify.css";
import InspectorSidebar from "./inspector_Sidebar";

function calculateLandPrice(totalAreaInSquareMeters) {
    const fixedPricePerSquareMeter = 0.01;
    return totalAreaInSquareMeters * fixedPricePerSquareMeter;
}

const VerifyLand = () => {
    const [landData, setLandData] = useState([]);
    const [inspectorCount, setInspectorCount] = useState(0);
    const [landCount, setLandCount] = useState(0);

    useEffect(() => {
        const fetchLandData = async () => {
            if (window.ethereum) {
                const web3 = new Web3(window.ethereum);
                try {
                    await window.ethereum.enable();
                    const accounts = await web3.eth.getAccounts();
                    const account = accounts[0];

                    const landChainContract = new web3.eth.Contract(LandChain_ABI, LandChain);

                    const inspectorCount = await landChainContract.methods.inspectorsCount().call();
                    setInspectorCount(inspectorCount);

                    const landCount = await landChainContract.methods.landsCount().call();
                    setLandCount(landCount);

                    const lands = [];

                    for (let i = landCount; i > 0; i--) {
                        const land = await landChainContract.methods.lands(i).call();
                        land.price = calculateLandPrice(land.totalArea);
                        lands.push(land);
                    }

                    setLandData(lands);
                } catch (error) {
                    console.error("Error fetching land data: ", error);
                }
            } else {
                console.error("MetaMask is not installed.");
            }
        };

        fetchLandData();
    }, []);

    return (
        <div>
            <div className='land-side-bar-header'>
                <h2>Inspector Dashboard</h2>
            </div>
            <InspectorSidebar />

            <div className="land-table-container">
                <div className="stats">
                    <h2>Yearly Performance</h2>
                    <h2>Total Land Inspector: {inspectorCount}</h2>
                    <h2>Total Land Count: {landCount}</h2>
                </div>
                <h2>Land Information Registered</h2>
                <div className="table-scrollable">
                    <table className="land-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Owner Address</th>
                                <th>Price in Eth</th>
                                <th>Total Area (sq. m)</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {landData.map((land, index) => (
                                !land.declined && (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{land.owner}</td>
                                        <td>{land.price}</td>
                                        <td>{land.totalArea}</td>
                                        <td>
                                            <Link to={`/detail/${land.thramNumber}`}>
                                                <button className="view-detail">Details</button>
                                            </Link>
                                        </td>
                                    </tr>
                                )
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default VerifyLand;
