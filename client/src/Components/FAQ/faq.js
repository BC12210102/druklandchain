import React, { useEffect } from "react";
import { Icon } from "@iconify/react";
import Navigation from "../Landing/Navigation";
import logo from './image/footerlogo.png';
import Footer from "../Landing/footer";
import AOS from 'aos';
import 'aos/dist/aos.css';
import "./faq.css";

const FAQ = () => {
    useEffect(() => {
        AOS.init({
            duration: 1000, // Duration of the animation
            easing: 'ease-in-out', // Easing option
            once: true // Animation only occurs once
        });
    }, []);

    return (
        <div className="faq_hero">
            <Navigation />
            <div className='logo' data-aos="fade-right">
                <img src={logo} className="logo" alt='logo' height={50} />
            </div>

            <div className='faq_text' data-aos="fade-left">
                <p>
                    "Got questions? We've got answers! Explore our frequently asked questions to learn more about our platform,
                    services, and how we can help you navigate the world of land transactions with confidence and ease."
                </p>
            </div>
            <div className='faq_search' data-aos="fade-up">
                <input placeholder="search..." type="text"></input>
                <button>
                    <Icon icon="bi:arrow-right" className="right-arrow" />
                </button>
            </div>
            <div className="accordion" data-aos="fade-up">
                <h2>Frequently Asked Questions</h2>
                <div className="accordion-item">
                    <input type="checkbox" id="accordion2" />
                    <label htmlFor="accordion2" className="accordion-item-title">
                        <span className="icon"></span>Is DrukLandChain secure?

                    </label>
                    <div className="accordion-item-desc">Yes, security is our top priority at DrukLandChain. We utilize advanced encryption and security protocols to ensure the safety of your data and transactions.</div>
                </div>

                <div className="accordion-item">
                    <input type="checkbox" id="accordion3" />
                    <label htmlFor="accordion3" className="accordion-item-title">
                        <span className="icon"></span>How can I buy land through DrukLandChain?
                    </label>
                    <div className="accordion-item-desc">To buy land through DrukLandChain, simply sign up for an account, browse available listings, and connect with sellers. Our platform provides tools and resources to help you find the perfect piece of land for your needs.</div>
                </div>

                <div className="accordion-item">
                    <input type="checkbox" id="accordion4" />
                    <label htmlFor="accordion4" className="accordion-item-title">
                        <span className="icon"></span>How do I sell land on DrukLandChain?
                    </label>
                    <div className="accordion-item-desc">First, if a user wants to sell his/her land he/she needs to register into the system. After registering into the system they can submit the land details and wait for land verification. After their registered details is being verified then they can sell their land on marketplace.</div>
                </div>

                <div className="accordion-item">
                    <input type="checkbox" id="accordion5" />
                    <label htmlFor="accordion5" className="accordion-item-title">
                        <span className="icon"></span>What fees are involved in using DrukLandChain?
                    </label>
                    <div className="accordion-item-desc">First one is a user must pay some gas fees while invoking the metamask, for registration. Secondly, user will be provided with the token in which they can exchange the token. Thirdly, a user will have rights to exchange their token with ether.</div>
                </div>
                <div className="accordion-item">
                    <input type="checkbox" id="accordion6" /> {/* Unique id for the second-last question */}
                    <label htmlFor="accordion6" className="accordion-item-title">
                        <span className="icon"></span>Is DrukLandChain available in my area?
                    </label>
                    <div className="accordion-item-desc">As our scope is within Bhutan only, yes, DrukLandChain is available if you are within Bhutan.</div>
                </div>
                <div className="accordion-item">
                    <input type="checkbox" id="accordion7" /> {/* Unique id for the second-last question */}
                    <label htmlFor="accordion7" className="accordion-item-title">
                        <span className="icon"></span>How can i get support or assistance to DrukLandChain?
                    </label>
                    <div className="accordion-item-desc"> In order to get support and assistance about our system you can kindly email us.</div>
                </div>

            </div>
            <div className="faq-footer" data-aos="fade-up">
                <Footer />
            </div>
        </div>
    )
}
export default FAQ;