module.exports = {
  networks: {
    development: {
     host: "host.docker.internal",
     port: 7545,
     network_id: "*",
     gas: 100, // Increased gas limit
    },
  },
  compilers: {
    solc: {
      version: "^0.8.25",
      settings: {
        optimizer: {
          enabled: true, // Enable optimizer
          runs: 200
        },
      },
    },
  },
};
// const fs = require('fs');
// const HDWalletProvider = require('@truffle/hdwallet-provider');
// const privateKey = fs.readFileSync(".secret").toString().trim();
// const infuraURL = "https://sepolia.infura.io/v3/f45bb76d84564214af4451e087e0dea7";

module.exports = {
  networks: {
    development: {
      // host: "127.0.0.1",     // Localhost (default: none)
      host: "host.docker.internal",     // Localhost (default: none)
      port: 7545,            // Standard Ethereum port (default: none)
      network_id: "*",       // Any network (default: none)
    },
    // sepolia: {
    //   provider: () => new HDWalletProvider(privateKey, infuraURL),
    //   network_id: 11155111,
    //   gas: 5500000,
    //   confirmations: 2,
    //   timeoutBlocks: 200,
    //   skipDryRun: true,
    //   networkCheckTimeout: 10000,
    //   timeoutBlocks: 200
    // }
  },

  mocha: {
    // timeout: 100000
  },

  compilers: {
    solc: {
      version: "0.8.0",    // Fetch exact version from solc-bin (default: truffle's version)
      // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
      // settings: {          // See the solidity docs for advice about optimization and evmVersion
      optimizer: {
        enabled: true,
        runs: 200
      },
      //  evmVersion: "byzantium"
      // }
    }
  },


  db: {
    enabled: false
  }
};
