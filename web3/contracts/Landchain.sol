// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract LandChain {
    address public contractOwner;
    uint public constant VOTE_PRICE = 0.1 ether;

    constructor() {
        contractOwner = msg.sender;
    }

    struct LandInspector {
        string name;
        string email;
        address walletAddr;
        uint voteCount;
        bool verified;
    }

    mapping(address => LandInspector) public inspectors;
    address[] public inspectorList;
    uint256 public inspectorsCount;

    modifier onlyOwner() {
        require(
            msg.sender == contractOwner,
            "Only contract owner can perform this action"
        );
        _;
    }

    event InspectorAdded(address indexed inspectorAddr, string name);
    event Voted(
        address indexed voter,
        address indexed inspectorAddr,
        uint value
    );
    event LandPurchased(uint landId, address buyer, address seller, uint price);
    event LandDeclined(uint thramNumber, address indexed inspectorAddr);
    event InspectorVerified(address inspector);
    event InspectorUpdated(
        address indexed inspectorAddr,
        string newName,
        string newEmail
    );

    function addLandInspector(
        string memory name,
        string memory email,
        address walletAddr
    ) public onlyOwner returns (bool) {
        require(
            inspectors[walletAddr].walletAddr == address(0),
            "Inspector already exists"
        );
        inspectorsCount++;
        inspectors[walletAddr] = LandInspector(
            name,
            email,
            walletAddr,
            0,
            false
        );
        inspectorList.push(walletAddr);
        emit InspectorAdded(walletAddr, name);
        return true;
    }

    function getAllInspectors() public view returns (LandInspector[] memory) {
        LandInspector[] memory allInspectors = new LandInspector[](
            inspectorList.length
        );
        for (uint256 i = 0; i < inspectorList.length; i++) {
            address addr = inspectorList[i];
            allInspectors[i] = inspectors[addr];
        }
        return allInspectors;
    }

    function voteInspector(address inspectorAddr) public payable {
        require(msg.value == VOTE_PRICE, "Vote price must be 0.1 ether");
        require(
            inspectors[inspectorAddr].walletAddr != address(0),
            "Inspector not found"
        );
        inspectors[inspectorAddr].voteCount++;
        emit Voted(msg.sender, inspectorAddr, msg.value);

        if (inspectors[inspectorAddr].voteCount >= 3) {
            inspectors[inspectorAddr].verified = true;
            emit InspectorVerified(inspectorAddr);
        }
    }

    struct Landreg {
        uint thramNumber;
        uint plotNumber;
        string location;
        uint totalArea;
        address owner;
        uint price;
        string maphash;
        string imagehash;
        bool verified;
        bool purchased;
    }

    uint public landsCount;
    mapping(uint => Landreg) public lands;
    mapping(address => uint[]) public ownerLands;

    event LandRegistered(uint landId, address owner, uint price);

    function addLand(
        uint _thramNumber,
        uint _plotNumber,
        string memory _location,
        uint _totalAreaInSquareMeters,
        string memory _maphash,
        string memory _imagehash
    ) public {
        landsCount++;
        uint landPrice = calculateLandPrice(_totalAreaInSquareMeters);

        lands[landsCount] = Landreg(
            _thramNumber,
            _plotNumber,
            _location,
            _totalAreaInSquareMeters,
            msg.sender,
            landPrice,
            _maphash,
            _imagehash,
            false,
            false
        );

        ownerLands[msg.sender].push(landsCount);

        emit LandRegistered(landsCount, msg.sender, landPrice);
    }

    function calculateLandPrice(
        uint _totalAreaInSquareMeters
    ) internal pure returns (uint) {
        uint fixedPricePerSquareMeter = 0.01 ether;
        return _totalAreaInSquareMeters * fixedPricePerSquareMeter;
    }

    function verifyLand(uint thramNumber) public {
        uint landId;
        bool landExists;
        for (uint i = 1; i <= landsCount; i++) {
            if (lands[i].thramNumber == thramNumber) {
                landId = i;
                landExists = true;
                break;
            }
        }
        require(landExists, "Land with the given Thram number does not exist");
        require(
            inspectors[msg.sender].walletAddr != address(0),
            "Caller is not an approved land inspector"
        );

        require(
            inspectors[msg.sender].verified == true,
            "Inspector is not verified"
        );

        lands[landId].verified = true;

        emit InspectorVerified(msg.sender);
    }

    function declineLand(uint thramNumber) public {
        uint landId;
        bool landExists;
        for (uint i = 1; i <= landsCount; i++) {
            if (lands[i].thramNumber == thramNumber) {
                landId = i;
                landExists = true;
                break;
            }
        }
        require(landExists, "Land with the given Thram number does not exist");

        address inspectorAddr = msg.sender;
        require(
            inspectors[inspectorAddr].walletAddr != address(0),
            "Caller is not an approved land inspector"
        );
        lands[landId] = lands[landsCount];
        delete lands[landsCount];
        landsCount--;

        emit LandDeclined(thramNumber, inspectorAddr);
    }

    function buyLand(uint thramNumber) public payable {
        uint landId = findLandId(thramNumber);
        require(landId != 0, "Land not found");
        Landreg storage land = lands[landId];
        require(msg.value >= land.price, "Insufficient funds to buy land");
        require(land.verified, "Land is not verified yet");
        require(!land.purchased, "Land is already purchased!");
        address payable landOwner = payable(land.owner);
        landOwner.transfer(land.price);
        address oldOwner = land.owner;
        land.owner = msg.sender;
        land.purchased = true;
        emit LandPurchased(landId, msg.sender, oldOwner, land.price);
    }

    function getPriceInEther(uint landId) public view returns (uint) {
        return lands[landId].price / 1 ether;
    }

    function convertWeiToEther(uint priceInWei) public pure returns (uint) {
        return priceInWei / 1 ether;
    }

    function getLandDetails(
        uint landId
    )
        public
        view
        returns (
            uint thramNumber,
            uint plotNumber,
            string memory location,
            uint totalArea,
            address owner,
            uint priceInEther,
            string memory maphash,
            string memory imagehash,
            bool verified
        )
    {
        Landreg memory land = lands[landId];
        thramNumber = land.thramNumber;
        plotNumber = land.plotNumber;
        location = land.location;
        totalArea = land.totalArea;
        owner = land.owner;
        priceInEther = land.price / 1 ether;
        maphash = land.maphash;
        imagehash = land.imagehash;
        verified = land.verified;
    }

    function getLandDetailsByThram(
        uint thramNumber
    )
        public
        view
        returns (
            uint plotNumber,
            string memory location,
            uint totalArea,
            address owner,
            uint priceInEther,
            string memory maphash,
            string memory imagehash,
            bool verified
        )
    {
        uint landId;
        bool landExists;

        for (uint i = 1; i <= landsCount; i++) {
            if (lands[i].thramNumber == thramNumber) {
                landId = i;
                landExists = true;
                break;
            }
        }

        require(landExists, "Land with the given Thram number does not exist");

        Landreg memory land = lands[landId];
        plotNumber = land.plotNumber;
        location = land.location;
        totalArea = land.totalArea;
        owner = land.owner;
        priceInEther = land.price / 1 ether;
        maphash = land.maphash;
        imagehash = land.imagehash;
        verified = land.verified;
    }

    function isVerifiedInspector(
        address walletAddr
    ) public view returns (bool) {
        return
            inspectors[walletAddr].walletAddr != address(0) &&
            inspectors[walletAddr].verified;
    }

    function removeInspector(address inspectorAddr) public onlyOwner {
        require(
            inspectors[inspectorAddr].walletAddr != address(0),
            "Inspector not found"
        );
        delete inspectors[inspectorAddr];
        for (uint i = 0; i < inspectorList.length; i++) {
            if (inspectorList[i] == inspectorAddr) {
                inspectorList[i] = inspectorList[inspectorList.length - 1];
                inspectorList.pop();
                break;
            }
        }
        inspectorsCount--;
    }

    function updateInspector(
        address inspectorAddr,
        string memory newName,
        string memory newEmail
    ) public onlyOwner {
        require(
            inspectors[inspectorAddr].walletAddr != address(0),
            "Inspector not found"
        );

        inspectors[inspectorAddr].name = newName;
        inspectors[inspectorAddr].email = newEmail;

        emit InspectorUpdated(inspectorAddr, newName, newEmail);
    }

    function getVerifiedLands() public view returns (Landreg[] memory) {
        Landreg[] memory verifiedLands = new Landreg[](landsCount);
        uint verifiedCount = 0;

        for (uint i = 1; i <= landsCount; i++) {
            if (lands[i].verified) {
                verifiedLands[verifiedCount] = lands[i];
                verifiedCount++;
            }
        }

        Landreg[] memory result = new Landreg[](verifiedCount);
        for (uint j = 0; j < verifiedCount; j++) {
            result[j] = verifiedLands[j];
        }

        return result;
    }

    function findLandId(uint256 thramNumber) internal view returns (uint256) {
        for (uint256 i = 1; i <= landsCount; i++) {
            if (lands[i].thramNumber == thramNumber) {
                return i;
            }
        }
        return 0;
    }

    function getLandsByOwner(
        address owner
    ) public view returns (Landreg[] memory) {
        uint[] memory landIds = ownerLands[owner];
        Landreg[] memory ownedLands = new Landreg[](landIds.length);

        for (uint i = 0; i < landIds.length; i++) {
            ownedLands[i] = lands[landIds[i]];
        }

        return ownedLands;
    }

    // getter functions
    function isInspectorVerified(
        address inspector
    ) internal view returns (bool) {
        return inspectors[inspector].verified;
    }

    function hasEnoughVotes(address inspector) internal view returns (bool) {
        return inspectors[inspector].voteCount >= 3;
    }

    function getLandOwner(uint thramNumber) public view returns (address) {
        uint landId = findLandId(thramNumber);
        require(landId != 0, "Land with the given Thram number does not exist");
        return lands[landId].owner;
    }

    function getLandPrice(uint thramNumber) public view returns (uint) {
        uint landId = findLandId(thramNumber);
        require(landId != 0, "Land with the given Thram number does not exist");
        return lands[landId].price;
    }

    function isLandVerified(uint thramNumber) public view returns (bool) {
        uint landId = findLandId(thramNumber);
        require(landId != 0, "Land with the given Thram number does not exist");
        return lands[landId].verified;
    }

    function checkLandBought(uint thramNumber) public view returns (bool) {
        uint landId = findLandId(thramNumber);
        require(landId != 0, "Land with the given Thram number does not exist");
        return lands[landId].purchased;
    }
}
